package org.bitbucket.vsazanovich.dailymoodlog;

import org.apache.commons.lang3.Range;
import org.bitbucket.vsazanovich.dailymoodlog.model.DMLProject;
import org.bitbucket.vsazanovich.dailymoodlog.ui.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 08-08-2019
 */
public class Constants {

  public static final Comparator SURVEYS_COMPARATOR = new Comparator<Long>() {
    @Override
    public int compare(Long o1, Long o2) {
      if (o1 < o2) {
        return 1;
      } else if (o1 > o2) {
        return -1;
      } else {
        return 0;
      }
    }
  };

  public static DMLProject CURRENT_PROJECT = new DMLProject();

  public static MainFrame FRAME;
  public static String TITLE = "Daily Mood Log";

  public static String DOWNWARD_TITLE = "Downward Arrow Technique";
  public static String COSTBENEFIT_TITLE = "Cost-Benefit Analysis (CBA)";
  public static String PLEASURE_TITLE = "Pleasure-Predicting Technique";
  public static String WHATIF_TITLE = "What-If Technique";
  public static String COMPASSION_TITLE = "Compassion-Based Technique";
  public static String EXAMINE_TITLE = "Examine The Evidence";
  public static String EXPERIMENTAL_TITLE = "Experimental Technique";
  public static String SURVEY_TITLE = "Survey Technique";
  public static String REATTRIBUTION_TITLE = "Reattribution";
  public static String SHADES_TITLE = "Thinking in Shades of Gray";
  public static String PROCESS_TITLE = "Process versus Outcome";
  public static String SEMANTIC_TITLE = "Semantic Method";
  public static String LETS_TITLE = "Let's Define Terms";
  public static String BE_TITLE = "Be Specific";
  public static String SELF_TITLE = "Self-Monitoring";
  public static String WORRY_TITLE = "Worry Breaks";
  public static String SHAME_TITLE = "Shame-Attacking Exercises";
  public static String PAR_TITLE = "Paradoxical Magnification";
  public static String HUM_TITLE = "Humorous Imaging";
  public static String EXT_TITLE = "Externalization of Voices";
  public static String ACC_TITLE = "Acceptance Paradox";
  public static String PARCBA_TITLE = "Paradoxical Cost-Benefit Analysis";
  public static String DEV_TITLE = "Devil's Advocate Technique";
  public static String LIT_TITLE = "Little Steps for Big Feats";
  public static String ANTI_TITLE = "Anti-Procrastination Sheet";
  public static String PRO_TITLE = "Problem-Solution List";
  public static String GRA_TITLE = "Gradual Exposure";
  public static String FLOOD_TITLE = "Flooding";
  public static String RESP_TITLE = "Response Prevention";
  public static String DIST_TITLE = "Distraction";
  public static String COG_TITLE = "Cognitive Flooding";
  public static String IMG_TITLE = "Image Substitution";
  public static String MEM_TITLE = "Memory Rescripting";
  public static String FEA_TITLE = "Feared Fantasy";
  public static String SMILE_TITLE = "Smile and Hello Practice";
  public static String FLIRT_TITLE = "Flirting Training";
  public static String REJ_TITLE = "Rejection Practice";
  public static String SELFD_TITLE = "Self-Disclosure";
  public static String DAVID_TITLE = "David Letterman Technique";
  public static String HIDDEN_TITLE = "Hidden Emotion Technique";

  public static String QUICK_TITLE = "Quick Help";

  public static final String DEFAULT_LAF = "com.jtattoo.plaf.aluminium.AluminiumLookAndFeel";//initial LAF

  public static Properties PROPS = new Properties();

  public static final String PROP_LAF = "PROP_LAF";

  public static final String PROP_WIDTH = "PROP_WIDTH";
  public static final String PROP_HEIGHT = "PROP_HEIGHT";
  public static final String PROP_POS_X = "PROP_POS_X";
  public static final String PROP_POS_Y = "PROP_POS_Y";
  public static final String PROP_DIVIDER_LOCATION = "PROP_DIVIDER_LOCATION";
  public static final String PROP_SELECTED_TAB = "PROP_SELECTED_TAB";
  public static final String PROP_LISTS_DIVIDER_LOCATION = "PROP_LISTS_DIVIDER_LOCATION";

  public static final String PROP_LAST_OPEN_FILE = "PROP_LAST_OPEN_FILE";
  public static final String PROP_LAST_OPEN_DIR = "PROP_LAST_OPEN_DIR";

  public static final String PROP_EDITOR_COMPONENT = "PROP_EDITOR_COMPONENT";
  public static final String PROP_WELCOME_COMPONENT = "PROP_WELCOME_COMPONENT";
  public static final String PROP_PREFS_COMPONENT = "PROP_PREFS_COMPONENT";

  public static final int BACKUP_LIMIT = 20;

  public static final Range RANGE1 = Range.between(0, 4);
  public static final Range RANGE2 = Range.between(5, 14);
  public static final Range RANGE3 = Range.between(15, 19);
  public static final Range RANGE4 = Range.between(20, 21);

  public static final String PROPS_FILE_NAME = "dailymoodlog.properties";


  public static final String INTER1 = "Normal Range \n0-1 Few or no symptoms of anxiety.\n\nThis is the best possible score. Right now you don't seem to be plagued by much, if any, anxiety or worry.";
  public static final String INTER2 = "Normal Range \n2-4 Borderline anxiety.\n\nAlthough you only have a few symptoms of anxiety, the tools in this program could be helpful.";
  public static final String INTER3 = "Clinical Range \n5-8 Mild anxiety.\n\nThis score isn't especially high, but the anxiety may be causing significant distress or discomfort.";
  public static final String INTER4 = "Clinical Range \n9-12 Moderate anxiety.\n\nYou've answered moderately or more on at least two of the items. This is definitely enough anxiety to cause distress, and there's lots of room for improvement.";
  public static final String INTER5 = "Clinical Range \n13-16 Severe anxiety.\n\nThis score indicates strong feelings of anxiety. you may be very uncomfortable, to say the least. The good news is that if you're willing to bring a little courage and hard work to the table, the prognosis is very positive.";
  public static final String INTER6 = "Clinical Range \n17-20 Extreme anxiety.\n\nYou seem to be plagued by intense feelings of anxiety, and you're probably suffering a great deal. The tools in this program, as well as professional therapy, could be extremely helpful.";

  public static final String INTER21 = "Few or no physical symptoms of anxiety.";
  public static final String INTER22 = "A few anxious physical symptoms.";
  public static final String INTER23 = "Mild anxious physical symptoms.";
  public static final String INTER24 = "Moderate anxious physical symptoms.";
  public static final String INTER25 = "Strong anxious physical symptoms.";
  public static final String INTER26 = "Extreme anxious physical symptoms.";

  public static final String INTER31 = "Normal Range\n0-1 Few or no symptoms of depression.\nThis is the best possible score. Depression doesn't seem to be a problem for you at this time. If you've been depressed, this is the score you eventually want to aim for. It may take a while to get there, but if you persist, you can make it happen.";
  public static final String INTER32 = "Normal Range\n2-4 Borderline depression.\nYou have minimal symptoms of depression and probably only need is mental tune-up. This score usually indicates normal ups and downs. Still, a score in this range is like having a low-grade fever. The fever isn't high, but you don't have nearly as much energy and motivation as you'd like. Furthermore, if you let a borderline depression persist, often it will worsen. Although this book focuses primarily on anxiety, many of the techniques can also be very helpful for depression.";
  public static final String INTER33 = "Clinical Range\n5-8 Mild depression.\nYou have several mild symptoms of depression or a couple stronger symptoms. Although this degree of depression is only mild, it's more than enough to rob you of self-esteem and joy in daily living.";
  public static final String INTER34 = "Clinical Range\n9-12 Moderate depression.\nThis is a significant level of depression. Many people have scores in this range for months or even years at a time. Although the prognosis for recovery is very positive, the sad thing is that many depressed individuals don't realize this or believe it. They feel like they really are defective or inferior and believe that its simply not in the cards for them to ever feel any real happiness or fulfillment. I hope you won't give in to that mind-set because it will operate as a self-fulfilling prophecy. Once you give up, nothing changes. Then you conclude that things really are hopeless. I'm convinced that every person who suffers from depression can recover and feel joy and self-esteem again.";
  public static final String INTER35 = "Clinical Range\n13-16 Severe depression.\nIndividuals who score in this range usually feel incredibly down, discouraged, and worthless and nothing seems rewarding or satisfying. This degree of suffering can be overwhelming and difficult for others to comprehend. When you try to tell your friends or family how you feel, they may tell you to cheer up and look at the bright side. Of course, that kind of superficial advice-giving usually makes things worse, because you feel discounted and rejected. In fact, the prognosis for recovery from severe depression is excellent, even though it doesn't feel that way right now.";
  public static final String INTER36 = "Clinical Range\n17-20 Extreme depression.\nThis degree of depression indicates almost unbelievable suffering, but the prognosis for improvement and recovery is still very positive. Although self-help techniques can be very useful, professional treatment may also be needed. It's a lot like being lost in the woods. You feel frightened because it getting dark and cold, and you don't know how to find your way home. Often a good guide whom you like and trust can show you the path that will lead you to safety.";

  public static final String INTER41 = "Item 1\n\nIf you're feeling depressed or discouraged, elevated scores on this item aren't unusual. Most depressed individuals have some suicidal thoughts at times. These thoughts aren't usually dangerous unless you have plans to act on them. However, it would be wise to consult with a mental health professional if you have any suicidal fantasies or urges or if the feelings of depression and discouragement have lasted more than a week or two. your life is precious, and you don't want to play Russian roulette with it.";
  public static final String INTER42 = "Item 2\n\nThis item asks about suicidal urges. Any score of 1 or higher can be dangerous and professional treatment is definitely indicated. If you have any desires to end your life, you should call 911 and seek emergency treatment immediately.";

  public static final SimpleDateFormat BMS_TIMEFORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");

  public static final String[] SDB_EXPLANATIONS = new String[]{
      "I must never fail or make a mistake.",
      "People won't love or accept me if I'm flawed or vulnerable.",
      "My worth as a human being depends on my achievements, intelligence, talent, status, income, or looks.",
      "I need every one's approval to be worthwhile.",
      "I can't feel happy and fullfilled without being loved. If I'm not loved, then life is not worth living.",
      "If you reject me, it proves that there's something wrong with me. If I'm alone, I'm bound to feel miserable and worthless.",
      "I should always try to please you, even if I make myself miserable in the process.",
      "People who love each other should never fight or argue.",
      "The problems in my relationships are bound to be my fault.",
      "The problems in my relationships are always the other person's fault.",
      "You should always trea me in the way I expect.",
      "I'm right and you're wrong.",
      "My problems could never be solved. I could never feel truly happy or fulfilled.",
      "I'm basically worthless, defective, and inferior to others.",
      "I should always feel happy, confident, and in control.",
      "Anger is dangerous and should be avoided at all costs.",
      "I should never feel sad, anxious, inadequate, jealous, or vulnerable. I should sweep my feelings under the rug and not upset anyone.",
      "The people I care about are demanding, manipulative, and powerful.",
      "People are clones who all think alike. If one person looks down on me, the word will spread like brushfire and soon everyone will.",
      "Talking to people is like having to perform under a bright spotlight. If I don't impress them by being sophisticated, witty, or interesting, the won't like me.",
      "If I worry enough, everything will turn out okay.",
      "I should never be frustrated. Life should always be easy.",
      "I should always be strong and never be weak."
  };

  public static Map<Integer, String> INTER1MAP = new HashMap<>();

  static {
    INTER1MAP.put(0, INTER1);
    INTER1MAP.put(1, INTER1);

    INTER1MAP.put(2, INTER2);
    INTER1MAP.put(3, INTER2);
    INTER1MAP.put(4, INTER2);

    INTER1MAP.put(5, INTER3);
    INTER1MAP.put(6, INTER3);
    INTER1MAP.put(7, INTER3);
    INTER1MAP.put(8, INTER3);

    INTER1MAP.put(9, INTER4);
    INTER1MAP.put(10, INTER4);
    INTER1MAP.put(11, INTER4);
    INTER1MAP.put(12, INTER4);

    INTER1MAP.put(13, INTER5);
    INTER1MAP.put(14, INTER5);
    INTER1MAP.put(15, INTER5);
    INTER1MAP.put(16, INTER5);

    INTER1MAP.put(17, INTER6);
    INTER1MAP.put(18, INTER6);
    INTER1MAP.put(19, INTER6);
    INTER1MAP.put(20, INTER6);
  }

  public static Map<Integer, String> INTER2MAP = new HashMap<>();

  static {
    INTER2MAP.put(0, INTER21);
    INTER2MAP.put(1, INTER21);
    INTER2MAP.put(2, INTER21);

    INTER2MAP.put(3, INTER22);
    INTER2MAP.put(4, INTER22);
    INTER2MAP.put(5, INTER22);
    INTER2MAP.put(6, INTER22);

    INTER2MAP.put(7, INTER23);
    INTER2MAP.put(8, INTER23);
    INTER2MAP.put(9, INTER23);
    INTER2MAP.put(10, INTER23);

    INTER2MAP.put(11, INTER24);
    INTER2MAP.put(12, INTER24);
    INTER2MAP.put(13, INTER24);
    INTER2MAP.put(14, INTER24);
    INTER2MAP.put(15, INTER24);
    INTER2MAP.put(16, INTER24);
    INTER2MAP.put(17, INTER24);
    INTER2MAP.put(18, INTER24);
    INTER2MAP.put(19, INTER24);
    INTER2MAP.put(20, INTER24);

    INTER2MAP.put(21, INTER25);
    INTER2MAP.put(22, INTER25);
    INTER2MAP.put(23, INTER25);
    INTER2MAP.put(24, INTER25);
    INTER2MAP.put(25, INTER25);
    INTER2MAP.put(26, INTER25);
    INTER2MAP.put(27, INTER25);
    INTER2MAP.put(28, INTER25);
    INTER2MAP.put(29, INTER25);
    INTER2MAP.put(30, INTER25);

    INTER2MAP.put(31, INTER26);
    INTER2MAP.put(32, INTER26);
    INTER2MAP.put(33, INTER26);
    INTER2MAP.put(34, INTER26);
    INTER2MAP.put(35, INTER26);
    INTER2MAP.put(36, INTER26);
    INTER2MAP.put(37, INTER26);
    INTER2MAP.put(38, INTER26);
    INTER2MAP.put(39, INTER26);
    INTER2MAP.put(40, INTER26);
  }

  public static Map<Integer, String> INTER3MAP = new HashMap<>();

  static {
    INTER3MAP.put(0, INTER31);
    INTER3MAP.put(1, INTER31);

    INTER3MAP.put(2, INTER32);
    INTER3MAP.put(3, INTER32);
    INTER3MAP.put(4, INTER32);

    INTER3MAP.put(5, INTER33);
    INTER3MAP.put(6, INTER33);
    INTER3MAP.put(7, INTER33);
    INTER3MAP.put(8, INTER33);

    INTER3MAP.put(9, INTER34);
    INTER3MAP.put(10, INTER34);
    INTER3MAP.put(11, INTER34);
    INTER3MAP.put(12, INTER34);

    INTER3MAP.put(13, INTER35);
    INTER3MAP.put(14, INTER35);
    INTER3MAP.put(15, INTER35);
    INTER3MAP.put(16, INTER35);

    INTER3MAP.put(17, INTER36);
    INTER3MAP.put(18, INTER36);
    INTER3MAP.put(19, INTER36);
    INTER3MAP.put(20, INTER36);
  }

  public static int getHash(String str) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
      messageDigest.update(str.getBytes());
      String encryptedString = new String(messageDigest.digest());
      return encryptedString.hashCode();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return str.hashCode();
  }

  public static int roundTo(int i) {
    return i / 5 * 5;
  }

  public static JTextArea getInfoArea(String text) {
    JTextArea textArea = new JTextArea(text);
    textArea.setMargin(new Insets(5, 5, 5, 5));
    textArea.setColumns(30);
    textArea.setRows(10);
    textArea.setEditable(false);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setSize(textArea.getPreferredSize().width, textArea.getPreferredSize().height);
    return textArea;
  }
}

