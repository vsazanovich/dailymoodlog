package org.bitbucket.vsazanovich.dailymoodlog.model;

import org.bitbucket.vsazanovich.dailymoodlog.Constants;

import java.io.Serializable;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 15-10-2019
 */
public class DMLProject implements Serializable {
  private SortedMap<Long, BriefMoodSurvey> surveys = new TreeMap<Long, BriefMoodSurvey>(Constants.SURVEYS_COMPARATOR);
  private SortedMap<Long, UpsettingEvent> events = new TreeMap<Long, UpsettingEvent>(Constants.SURVEYS_COMPARATOR);
  private SortedMap<Long, NegativeThought> thoughts = new TreeMap<Long, NegativeThought>(Constants.SURVEYS_COMPARATOR);

  public SortedMap<Long, BriefMoodSurvey> getSurveys() {
    return surveys;
  }

  public void setSurveys(SortedMap<Long, BriefMoodSurvey> surveys) {
    this.surveys = surveys;
  }

  public SortedMap<Long, UpsettingEvent> getEvents() {
    return events;
  }

  public void setEvents(SortedMap<Long, UpsettingEvent> events) {
    this.events = events;
  }

  public SortedMap<Long, NegativeThought> getThoughts() {
    return thoughts;
  }

  public void setThoughts(SortedMap<Long, NegativeThought> thoughts) {
    this.thoughts = thoughts;
  }
}
