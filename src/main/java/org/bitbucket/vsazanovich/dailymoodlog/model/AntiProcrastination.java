package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;
import java.util.SortedMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 22-11-2019
 */
public class AntiProcrastination implements Serializable {
  private String task;
  private SortedMap<Integer,Subtask> subtasks;

  public String getTask() {
    return task;
  }

  public void setTask(String task) {
    this.task = task;
  }

  public SortedMap<Integer, Subtask> getSubtasks() {
    return subtasks;
  }

  public void setSubtasks(SortedMap<Integer, Subtask> subtasks) {
    this.subtasks = subtasks;
  }
}
