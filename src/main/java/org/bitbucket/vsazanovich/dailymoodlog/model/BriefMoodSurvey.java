package org.bitbucket.vsazanovich.dailymoodlog.model;

import org.apache.commons.lang3.Range;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;

import java.io.Serializable;
import java.util.Date;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 15-10-2019
 */
public class BriefMoodSurvey implements Serializable {
  private long time;
  private int[] answers;

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public int[] getAnswers() {
    return answers;
  }

  public void setAnswers(int[] answers) {
    this.answers = answers;
  }

  @Override
  public String toString() {
    return String.valueOf(Constants.BMS_TIMEFORMAT.format(new Date(time))) + " - " +
        getRes((Integer) Constants.RANGE1.getMinimum(), (Integer) Constants.RANGE1.getMaximum()) + " / " +
        getRes((Integer) Constants.RANGE2.getMinimum(), (Integer) Constants.RANGE2.getMaximum()) + " / " +
        getRes((Integer) Constants.RANGE3.getMinimum(), (Integer) Constants.RANGE3.getMaximum()) + " / " +
        getRes((Integer) Constants.RANGE4.getMinimum(), (Integer) Constants.RANGE4.getMaximum());
  }


  private String getRes(int from, int to) {
    int total = 0;
    for (int i = from; i <= to; i++) {
      total += answers[i] - 1 < 0 ? 0 : answers[i] - 1;
      if (answers[i] == 0) {
        //no selection
        return "-";
      }
    }
    return String.valueOf(total);
  }

  public int getResInt(Range range) {
    int total = 0;
    for (int i = (int) range.getMinimum(); i <= (int) range.getMaximum(); i++) {
      total += answers[i] - 1 < 0 ? 0 : answers[i] - 1;
      if (answers[i] == 0) {
        //no selection
        return 0;
      }
    }
    return total;
  }
}
