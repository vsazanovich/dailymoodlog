package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 22-11-2019
 */
public class ProblemSolution implements Serializable {
  private String problems;
  private String solutions;

  public String getProblems() {
    return problems;
  }

  public void setProblems(String problems) {
    this.problems = problems;
  }

  public String getSolutions() {
    return solutions;
  }

  public void setSolutions(String solutions) {
    this.solutions = solutions;
  }
}
