package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 23-10-2019
 */
public class CostBenefitAnalysis implements Serializable {
  private String changeTitle;
  private String advantages;
  private String disadvantages;
  private int adv;
  private int dis;
  private String revision;

  public String getChangeTitle() {
    return changeTitle;
  }

  public void setChangeTitle(String changeTitle) {
    this.changeTitle = changeTitle;
  }

  public String getAdvantages() {
    return advantages;
  }

  public void setAdvantages(String advantages) {
    this.advantages = advantages;
  }

  public String getDisadvantages() {
    return disadvantages;
  }

  public void setDisadvantages(String disadvantages) {
    this.disadvantages = disadvantages;
  }

  public int getAdv() {
    return adv;
  }

  public void setAdv(int adv) {
    this.adv = adv;
  }

  public int getDis() {
    return dis;
  }

  public void setDis(int dis) {
    this.dis = dis;
  }

  public String getRevision() {
    return revision;
  }

  public void setRevision(String revision) {
    this.revision = revision;
  }
}
