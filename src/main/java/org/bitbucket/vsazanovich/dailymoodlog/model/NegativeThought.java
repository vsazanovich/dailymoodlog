package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 18-10-2019
 */
public class NegativeThought implements Serializable {

  private DownwardArrow downwardArrow;
  private CostBenefitAnalysis costBenefitAnalysis;
  private PleasurePredictingSheet pleasurePredictingSheet;
  private CommonDataHolder whatIfTechniqueForm;
  private CommonDataHolder compassionBasedTechniqueForm;
  private CommonDataHolder examineTheEvidenceForm;
  private CommonDataHolder experimentalTechniqueForm;
  private CommonDataHolder surveyTechniqueForm;
  private CommonDataHolder reattributionForm;
  private CommonDataHolder thinkingInShadesOfGrayForm;
  private CommonDataHolder processVersusOutcomeForm;
  private SemanticMethod semanticMethodForm;
  private CommonDataHolder letsDefineTermsForm;
  private CommonDataHolder beSpecificForm;
  private CommonDataHolder selfMonitoringForm;
  private CommonDataHolder worryBreaksForm;
  private CommonDataHolder shameAttackingForm;
  private CommonDataHolder paradoxicalMagnificationForm;
  private CommonDataHolder humorousImagingForm;
  private CommonDataHolder externalizationOfVoicesForm;
  private CommonDataHolder acceptanceParadoxForm;
  private CommonDataHolder paradoxicalCostBenefitAnalysisForm;
  private CommonDataHolder devilsAdvocateForm;
  private CommonDataHolder littleStepsForBigFeatsForm;
  private AntiProcrastination antiProcrastinationSheetForm;
  private ProblemSolution problemSolutionListForm;
  private GradualExposure gradualExposureForm;
  private CommonDataHolder floodingForm;
  private CommonDataHolder responsePreventionForm;
  private CommonDataHolder distractionForm;
  private CommonDataHolder cognitiveFloodingForm;
  private CommonDataHolder imageSubstitutionForm;
  private CommonDataHolder memoryRescriptingForm;
  private CommonDataHolder fearedFantasyForm;
  private CommonDataHolder smileAndHelloPracticeForm;
  private CommonDataHolder flirtingTrainingForm;
  private CommonDataHolder rejectionPracticeForm;
  private CommonDataHolder selfDisclosureForm;
  private CommonDataHolder davidLettermanTechniqueForm;
  private CommonDataHolder hiddenEmotionTechniqueForm;

  private long time;
  private String title;

  public PleasurePredictingSheet getPleasurePredictingSheet() {
    return pleasurePredictingSheet;
  }

  public void setPleasurePredictingSheet(PleasurePredictingSheet pleasurePredictingSheet) {
    this.pleasurePredictingSheet = pleasurePredictingSheet;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public CostBenefitAnalysis getCostBenefitAnalysis() {
    return costBenefitAnalysis;
  }

  public void setCostBenefitAnalysis(CostBenefitAnalysis costBenefitAnalysis) {
    this.costBenefitAnalysis = costBenefitAnalysis;
  }

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public DownwardArrow getDownwardArrow() {
    return downwardArrow;
  }

  public void setDownwardArrow(DownwardArrow downwardArrow) {
    this.downwardArrow = downwardArrow;
  }

  @Override
  public String toString() {
    return title;
  }

  public CommonDataHolder getWhatIfTechniqueForm() {
    return whatIfTechniqueForm;
  }

  public void setWhatIfTechniqueForm(CommonDataHolder whatIfTechniqueForm) {
    this.whatIfTechniqueForm = whatIfTechniqueForm;
  }

  public CommonDataHolder getCompassionBasedTechniqueForm() {
    return compassionBasedTechniqueForm;
  }

  public void setCompassionBasedTechniqueForm(CommonDataHolder compassionBasedTechniqueForm) {
    this.compassionBasedTechniqueForm = compassionBasedTechniqueForm;
  }

  public CommonDataHolder getExamineTheEvidenceForm() {
    return examineTheEvidenceForm;
  }

  public void setExamineTheEvidenceForm(CommonDataHolder examineTheEvidenceForm) {
    this.examineTheEvidenceForm = examineTheEvidenceForm;
  }

  public CommonDataHolder getExperimentalTechniqueForm() {
    return experimentalTechniqueForm;
  }

  public void setExperimentalTechniqueForm(CommonDataHolder experimentalTechniqueForm) {
    this.experimentalTechniqueForm = experimentalTechniqueForm;
  }

  public CommonDataHolder getSurveyTechniqueForm() {
    return surveyTechniqueForm;
  }

  public void setSurveyTechniqueForm(CommonDataHolder surveyTechniqueForm) {
    this.surveyTechniqueForm = surveyTechniqueForm;
  }

  public CommonDataHolder getReattributionForm() {
    return reattributionForm;
  }

  public void setReattributionForm(CommonDataHolder reattributionForm) {
    this.reattributionForm = reattributionForm;
  }

  public CommonDataHolder getThinkingInShadesOfGrayForm() {
    return thinkingInShadesOfGrayForm;
  }

  public void setThinkingInShadesOfGrayForm(CommonDataHolder thinkingInShadesOfGrayForm) {
    this.thinkingInShadesOfGrayForm = thinkingInShadesOfGrayForm;
  }

  public CommonDataHolder getProcessVersusOutcomeForm() {
    return processVersusOutcomeForm;
  }

  public void setProcessVersusOutcomeForm(CommonDataHolder processVersusOutcomeForm) {
    this.processVersusOutcomeForm = processVersusOutcomeForm;
  }

  public SemanticMethod getSemanticMethodForm() {
    return semanticMethodForm;
  }

  public void setSemanticMethodForm(SemanticMethod semanticMethodForm) {
    this.semanticMethodForm = semanticMethodForm;
  }

  public CommonDataHolder getLetsDefineTermsForm() {
    return letsDefineTermsForm;
  }

  public void setLetsDefineTermsForm(CommonDataHolder letsDefineTermsForm) {
    this.letsDefineTermsForm = letsDefineTermsForm;
  }

  public CommonDataHolder getBeSpecificForm() {
    return beSpecificForm;
  }

  public void setBeSpecificForm(CommonDataHolder beSpecificForm) {
    this.beSpecificForm = beSpecificForm;
  }

  public CommonDataHolder getSelfMonitoringForm() {
    return selfMonitoringForm;
  }

  public void setSelfMonitoringForm(CommonDataHolder selfMonitoringForm) {
    this.selfMonitoringForm = selfMonitoringForm;
  }

  public CommonDataHolder getWorryBreaksForm() {
    return worryBreaksForm;
  }

  public void setWorryBreaksForm(CommonDataHolder worryBreaksForm) {
    this.worryBreaksForm = worryBreaksForm;
  }

  public CommonDataHolder getShameAttackingForm() {
    return shameAttackingForm;
  }

  public void setShameAttackingForm(CommonDataHolder shameAttackingForm) {
    this.shameAttackingForm = shameAttackingForm;
  }

  public CommonDataHolder getParadoxicalMagnificationForm() {
    return paradoxicalMagnificationForm;
  }

  public void setParadoxicalMagnificationForm(CommonDataHolder paradoxicalMagnificationForm) {
    this.paradoxicalMagnificationForm = paradoxicalMagnificationForm;
  }

  public CommonDataHolder getHumorousImagingForm() {
    return humorousImagingForm;
  }

  public void setHumorousImagingForm(CommonDataHolder humorousImagingForm) {
    this.humorousImagingForm = humorousImagingForm;
  }

  public CommonDataHolder getExternalizationOfVoicesForm() {
    return externalizationOfVoicesForm;
  }

  public void setExternalizationOfVoicesForm(CommonDataHolder externalizationOfVoicesForm) {
    this.externalizationOfVoicesForm = externalizationOfVoicesForm;
  }

  public CommonDataHolder getAcceptanceParadoxForm() {
    return acceptanceParadoxForm;
  }

  public void setAcceptanceParadoxForm(CommonDataHolder acceptanceParadoxForm) {
    this.acceptanceParadoxForm = acceptanceParadoxForm;
  }

  public CommonDataHolder getParadoxicalCostBenefitAnalysisForm() {
    return paradoxicalCostBenefitAnalysisForm;
  }

  public void setParadoxicalCostBenefitAnalysisForm(CommonDataHolder paradoxicalCostBenefitAnalysisForm) {
    this.paradoxicalCostBenefitAnalysisForm = paradoxicalCostBenefitAnalysisForm;
  }

  public CommonDataHolder getDevilsAdvocateForm() {
    return devilsAdvocateForm;
  }

  public void setDevilsAdvocateForm(CommonDataHolder devilsAdvocateForm) {
    this.devilsAdvocateForm = devilsAdvocateForm;
  }

  public CommonDataHolder getLittleStepsForBigFeatsForm() {
    return littleStepsForBigFeatsForm;
  }

  public void setLittleStepsForBigFeatsForm(CommonDataHolder littleStepsForBigFeatsForm) {
    this.littleStepsForBigFeatsForm = littleStepsForBigFeatsForm;
  }

  public AntiProcrastination getAntiProcrastinationSheetForm() {
    return antiProcrastinationSheetForm;
  }

  public void setAntiProcrastinationSheetForm(AntiProcrastination antiProcrastinationSheetForm) {
    this.antiProcrastinationSheetForm = antiProcrastinationSheetForm;
  }

  public ProblemSolution getProblemSolutionListForm() {
    return problemSolutionListForm;
  }

  public void setProblemSolutionListForm(ProblemSolution problemSolutionListForm) {
    this.problemSolutionListForm = problemSolutionListForm;
  }

  public GradualExposure getGradualExposureForm() {
    return gradualExposureForm;
  }

  public void setGradualExposureForm(GradualExposure gradualExposureForm) {
    this.gradualExposureForm = gradualExposureForm;
  }

  public CommonDataHolder getFloodingForm() {
    return floodingForm;
  }

  public void setFloodingForm(CommonDataHolder floodingForm) {
    this.floodingForm = floodingForm;
  }

  public CommonDataHolder getResponsePreventionForm() {
    return responsePreventionForm;
  }

  public void setResponsePreventionForm(CommonDataHolder responsePreventionForm) {
    this.responsePreventionForm = responsePreventionForm;
  }

  public CommonDataHolder getDistractionForm() {
    return distractionForm;
  }

  public void setDistractionForm(CommonDataHolder distractionForm) {
    this.distractionForm = distractionForm;
  }

  public CommonDataHolder getCognitiveFloodingForm() {
    return cognitiveFloodingForm;
  }

  public void setCognitiveFloodingForm(CommonDataHolder cognitiveFloodingForm) {
    this.cognitiveFloodingForm = cognitiveFloodingForm;
  }

  public CommonDataHolder getImageSubstitutionForm() {
    return imageSubstitutionForm;
  }

  public void setImageSubstitutionForm(CommonDataHolder imageSubstitutionForm) {
    this.imageSubstitutionForm = imageSubstitutionForm;
  }

  public CommonDataHolder getMemoryRescriptingForm() {
    return memoryRescriptingForm;
  }

  public void setMemoryRescriptingForm(CommonDataHolder memoryRescriptingForm) {
    this.memoryRescriptingForm = memoryRescriptingForm;
  }

  public CommonDataHolder getFearedFantasyForm() {
    return fearedFantasyForm;
  }

  public void setFearedFantasyForm(CommonDataHolder fearedFantasyForm) {
    this.fearedFantasyForm = fearedFantasyForm;
  }

  public CommonDataHolder getSmileAndHelloPracticeForm() {
    return smileAndHelloPracticeForm;
  }

  public void setSmileAndHelloPracticeForm(CommonDataHolder smileAndHelloPracticeForm) {
    this.smileAndHelloPracticeForm = smileAndHelloPracticeForm;
  }

  public CommonDataHolder getFlirtingTrainingForm() {
    return flirtingTrainingForm;
  }

  public void setFlirtingTrainingForm(CommonDataHolder flirtingTrainingForm) {
    this.flirtingTrainingForm = flirtingTrainingForm;
  }

  public CommonDataHolder getRejectionPracticeForm() {
    return rejectionPracticeForm;
  }

  public void setRejectionPracticeForm(CommonDataHolder rejectionPracticeForm) {
    this.rejectionPracticeForm = rejectionPracticeForm;
  }

  public CommonDataHolder getSelfDisclosureForm() {
    return selfDisclosureForm;
  }

  public void setSelfDisclosureForm(CommonDataHolder selfDisclosureForm) {
    this.selfDisclosureForm = selfDisclosureForm;
  }

  public CommonDataHolder getDavidLettermanTechniqueForm() {
    return davidLettermanTechniqueForm;
  }

  public void setDavidLettermanTechniqueForm(CommonDataHolder davidLettermanTechniqueForm) {
    this.davidLettermanTechniqueForm = davidLettermanTechniqueForm;
  }

  public CommonDataHolder getHiddenEmotionTechniqueForm() {
    return hiddenEmotionTechniqueForm;
  }

  public void setHiddenEmotionTechniqueForm(CommonDataHolder hiddenEmotionTechniqueForm) {
    this.hiddenEmotionTechniqueForm = hiddenEmotionTechniqueForm;
  }
}
