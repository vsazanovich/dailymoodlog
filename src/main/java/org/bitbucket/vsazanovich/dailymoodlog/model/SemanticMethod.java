package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 22-11-2019
 */
public class SemanticMethod implements Serializable {
  private String shoulds;
  private String substitutions;

  public String getShoulds() {
    return shoulds;
  }

  public void setShoulds(String shoulds) {
    this.shoulds = shoulds;
  }

  public String getSubstitutions() {
    return substitutions;
  }

  public void setSubstitutions(String substitutions) {
    this.substitutions = substitutions;
  }
}
