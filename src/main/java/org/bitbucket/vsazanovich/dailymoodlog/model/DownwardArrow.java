package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 18-10-2019
 */
public class DownwardArrow implements Serializable {
  private String thoughts;
  private int sdbChoice = -1;
  private String yourOwn;
  private String yourOwnExplanation;

  public String getThoughts() {
    return thoughts;
  }

  public void setThoughts(String thoughts) {
    this.thoughts = thoughts;
  }

  public int getSdbChoice() {
    return sdbChoice;
  }

  public void setSdbChoice(int sdbChoice) {
    this.sdbChoice = sdbChoice;
  }

  public String getYourOwn() {
    return yourOwn;
  }

  public void setYourOwn(String yourOwn) {
    this.yourOwn = yourOwn;
  }

  public String getYourOwnExplanation() {
    return yourOwnExplanation;
  }

  public void setYourOwnExplanation(String yourOwnExplanation) {
    this.yourOwnExplanation = yourOwnExplanation;
  }

  public DownwardArrow getData() {
      return null;
  }
}
