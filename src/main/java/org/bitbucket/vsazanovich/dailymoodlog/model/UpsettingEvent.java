package org.bitbucket.vsazanovich.dailymoodlog.model;

import org.bitbucket.vsazanovich.dailymoodlog.Constants;

import java.io.Serializable;
import java.util.Date;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 17-10-2019
 */
public class UpsettingEvent implements Serializable {
  private String title;
  private long time;
  private boolean[] emotions;
  private String otherEmotions;
  private String negativeThoughts;
  private String positiveThoughts;
  private int before;
  private int after;
  private int belief;
  private boolean[] distortions;

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean[] getEmotions() {
    return emotions;
  }

  public void setEmotions(boolean[] emotions) {
    this.emotions = emotions;
  }

  public String getOtherEmotions() {
    return otherEmotions;
  }

  public void setOtherEmotions(String otherEmotions) {
    this.otherEmotions = otherEmotions;
  }

  public String getNegativeThoughts() {
    return negativeThoughts;
  }

  public void setNegativeThoughts(String negativeThoughts) {
    this.negativeThoughts = negativeThoughts;
  }

  public String getPositiveThoughts() {
    return positiveThoughts;
  }

  public void setPositiveThoughts(String positiveThoughts) {
    this.positiveThoughts = positiveThoughts;
  }

  public int getBefore() {
    return before;
  }

  public void setBefore(int before) {
    this.before = before;
  }

  public int getAfter() {
    return after;
  }

  public void setAfter(int after) {
    this.after = after;
  }

  public int getBelief() {
    return belief;
  }

  public void setBelief(int belief) {
    this.belief = belief;
  }

  public boolean[] getDistortions() {
    return distortions;
  }

  public void setDistortions(boolean[] distortions) {
    this.distortions = distortions;
  }

  @Override
  public String toString() {
    return title;
  }
}
