package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.awt.*;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 24-10-2019
 */
public interface Datable {
  public Object getData();
  public Component getMainPanel();
}
