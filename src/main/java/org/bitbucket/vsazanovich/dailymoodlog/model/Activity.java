package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 23-10-2019
 */
public class Activity implements Serializable {
  private String name;
  private String companion;
  private int predictedSatisfaction;
  private int perfection;
  private int actualSatisfaction;
  private String notes;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCompanion() {
    return companion;
  }

  public void setCompanion(String companion) {
    this.companion = companion;
  }

  public int getPredictedSatisfaction() {
    return predictedSatisfaction;
  }

  public void setPredictedSatisfaction(int predictedSatisfaction) {
    this.predictedSatisfaction = predictedSatisfaction;
  }

  public int getPerfection() {
    return perfection;
  }

  public void setPerfection(int perfection) {
    this.perfection = perfection;
  }

  public int getActualSatisfaction() {
    return actualSatisfaction;
  }

  public void setActualSatisfaction(int actualSatisfaction) {
    this.actualSatisfaction = actualSatisfaction;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }
}
