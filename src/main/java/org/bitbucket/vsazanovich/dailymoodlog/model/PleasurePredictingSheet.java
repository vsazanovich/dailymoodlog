package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;
import java.util.Map;
import java.util.SortedMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 23-10-2019
 */
public class PleasurePredictingSheet implements Serializable {
  private String belief;
  private SortedMap<Integer, Activity> activities;

  public String getBelief() {
    return belief;
  }

  public void setBelief(String belief) {
    this.belief = belief;
  }

  public SortedMap<Integer, Activity> getActivities() {
    return activities;
  }

  public void setActivities(SortedMap<Integer, Activity> activities) {
    this.activities = activities;
  }
}
