package org.bitbucket.vsazanovich.dailymoodlog.model;

import java.io.Serializable;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 22-11-2019
 */
public class Subtask implements Serializable {
  private String name;
  private int predictedDifficulty;
  private int predictedSatisfaction;
  private int actualDifficulty;
  private int actualSatisfaction;
  private String notes;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPredictedDifficulty() {
    return predictedDifficulty;
  }

  public void setPredictedDifficulty(int predictedDifficulty) {
    this.predictedDifficulty = predictedDifficulty;
  }

  public int getPredictedSatisfaction() {
    return predictedSatisfaction;
  }

  public void setPredictedSatisfaction(int predictedSatisfaction) {
    this.predictedSatisfaction = predictedSatisfaction;
  }

  public int getActualDifficulty() {
    return actualDifficulty;
  }

  public void setActualDifficulty(int actualDifficulty) {
    this.actualDifficulty = actualDifficulty;
  }

  public int getActualSatisfaction() {
    return actualSatisfaction;
  }

  public void setActualSatisfaction(int actualSatisfaction) {
    this.actualSatisfaction = actualSatisfaction;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }
}
