package org.bitbucket.vsazanovich.dailymoodlog;

import org.bitbucket.vsazanovich.dailymoodlog.ui.MainFrame;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 30-11-2018
 */
public class DailyMoodLogLauncher {
  /**
   * Create the GUI and show it.  For thread safety,
   * this method should be invoked from the
   * event-dispatching thread.
   */
  private static void createAndShowGUI() {
    //Create and set up the window.
    Constants.FRAME = new MainFrame(Constants.TITLE);
    Constants.FRAME.pack();
    Constants.FRAME.setVisible(true);
  }

  public static void main(String[] args) {
    UIManager.put("Slider.focus", UIManager.get("Slider.background"));
    try {
      Constants.PROPS.load(new FileInputStream(System.getProperty("user.home") + File.separator + Constants.PROPS_FILE_NAME));
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }

    setLAF(Constants.PROPS.getProperty(Constants.PROP_LAF));

    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        createAndShowGUI();
      }
    });
  }

  public static void setLAF(String newLAF) {
    try {
      if (newLAF == null) {
        newLAF = Constants.DEFAULT_LAF;
      }
      System.out.println(newLAF);
      UIManager.setLookAndFeel(newLAF);
      if (Constants.FRAME != null) {
        SwingUtilities.updateComponentTreeUI(Constants.FRAME);
      }
      Constants.PROPS.setProperty(Constants.PROP_LAF, newLAF);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
