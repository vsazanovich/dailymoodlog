package org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.antiprocrastination;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.*;
import org.bitbucket.vsazanovich.dailymoodlog.ui.DnDTabbedPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 01-11-2019
 */
public class AntiProcrastinationSheetForm {

  public JPanel mainPanel;
  private JTextField textField1;
  private JPanel contentPanel;
  private JButton button1;
  private DnDTabbedPane dnd;
  private JDialog d;

  public AntiProcrastinationSheetForm(JDialog parent) {
    this.d = parent;
    dnd = new DnDTabbedPane(DnDTabbedPane.INDEX_TO_DECIMAL, "");
    contentPanel.add(dnd);
    dnd.addNewTab(null, AntiProcrastinationTaskForm.class);
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String text = "Break a large task down into small steps and predict how difficult and how satisfying you think each step will be on a scale from 0% to 100%.\n\n" +
            "After completing each step, record how difficult and satisfying it actually turned out to be, using the same scale.\n\n" +
            "Often you'll discover that each step is far easier and more rewarding than you expected.";
        JOptionPane.showMessageDialog(d, new JScrollPane(Constants.getInfoArea(text)),
            Constants.ANTI_TITLE,
            JOptionPane.PLAIN_MESSAGE);
      }
    });
  }

  @Override
  public String toString() {
    return Constants.ANTI_TITLE;
  }

  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer
   * >>> IMPORTANT!! <<<
   * DO NOT edit this method OR call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    final JPanel panel1 = new JPanel();
    panel1.setLayout(new BorderLayout(0, 0));
    mainPanel = new JPanel();
    mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
    panel1.add(mainPanel, BorderLayout.CENTER);
    contentPanel = new JPanel();
    contentPanel.setLayout(new BorderLayout(0, 0));
    mainPanel.add(contentPanel, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    final JPanel panel2 = new JPanel();
    panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
    mainPanel.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JPanel panel3 = new JPanel();
    panel3.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
    panel2.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    panel3.setBorder(BorderFactory.createTitledBorder("Task"));
    textField1 = new JTextField();
    panel3.add(textField1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    final JLabel label1 = new JLabel();
    Font label1Font = this.$$$getFont$$$(null, Font.ITALIC, -1, label1.getFont());
    if (label1Font != null) label1.setFont(label1Font);
    label1.setText("Break the task into small steps that you can complete in a few minutes.");
    panel3.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel4 = new JPanel();
    panel4.setLayout(new BorderLayout(0, 0));
    mainPanel.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JLabel label2 = new JLabel();
    Font label2Font = this.$$$getFont$$$(null, -1, 20, label2.getFont());
    if (label2Font != null) label2.setFont(label2Font);
    label2.setHorizontalAlignment(0);
    label2.setText("Anti-Procrastination Sheet");
    panel4.add(label2, BorderLayout.CENTER);
    button1 = new JButton();
    button1.setBorderPainted(false);
    button1.setContentAreaFilled(false);
    button1.setFocusPainted(false);
    button1.setFocusable(false);
    button1.setHorizontalTextPosition(0);
    button1.setIcon(new ImageIcon(getClass().getResource("/help-icon-11-24.png")));
    button1.setMaximumSize(new Dimension(32, 32));
    button1.setMinimumSize(new Dimension(32, 32));
    button1.setOpaque(false);
    button1.setPreferredSize(new Dimension(32, 32));
    button1.setText("");
    button1.setToolTipText("");
    button1.setVerifyInputWhenFocusTarget(false);
    panel4.add(button1, BorderLayout.EAST);
  }

  /**
   * @noinspection ALL
   */
  private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
    if (currentFont == null) return null;
    String resultName;
    if (fontName == null) {
      resultName = currentFont.getName();
    } else {
      Font testFont = new Font(fontName, Font.PLAIN, 10);
      if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
        resultName = fontName;
      } else {
        resultName = currentFont.getName();
      }
    }
    return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
  }

  public AntiProcrastination getData() {
    AntiProcrastination pps = new AntiProcrastination();
    pps.setTask(textField1.getText().trim());
    pps.setSubtasks(new TreeMap<>());
    for (int i = 0; i < dnd.getTabCount() - 1; i++) {
      Datable d = dnd.datables.get(i);
      pps.getSubtasks().put(i, (Subtask) d.getData());
    }
    return pps;
  }

  public void setData(AntiProcrastination pleasurePredictingSheet) {
    textField1.setText(pleasurePredictingSheet.getTask());
    if (pleasurePredictingSheet.getSubtasks().size() > 0) {
      for (int i = 0; i < dnd.getTabCount() - 1; i++) {
        dnd.removeTabAt(i);
      }
      dnd.datables.clear();
    }
    for (Subtask activity : pleasurePredictingSheet.getSubtasks().values()) {
      AntiProcrastinationTaskForm form = new AntiProcrastinationTaskForm();
      form.setData(activity);
      dnd.addNewTab(form, AntiProcrastinationTaskForm.class);
    }
    if (dnd.getTabCount() > 1) {
      dnd.setSelectedIndex(0);
    }
  }
}
