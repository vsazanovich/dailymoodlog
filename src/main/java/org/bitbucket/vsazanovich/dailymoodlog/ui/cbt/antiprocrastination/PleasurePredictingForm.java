package org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.antiprocrastination;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.Activity;
import org.bitbucket.vsazanovich.dailymoodlog.model.Datable;
import org.bitbucket.vsazanovich.dailymoodlog.model.PleasurePredictingSheet;
import org.bitbucket.vsazanovich.dailymoodlog.ui.DnDTabbedPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeMap;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 23-10-2019
 */
public class PleasurePredictingForm {
  public JPanel mainPanel;
  private JTextField textField1;
  private JPanel contentPanel;
  private JButton button1;
  private DnDTabbedPane dnd;
  private JDialog parent;

  public PleasurePredictingForm(JDialog p) {
    this.parent = p;
    dnd = new DnDTabbedPane(DnDTabbedPane.INDEX_TO_DECIMAL, "");
    contentPanel.add(dnd);
    dnd.addNewTab(null, PleasurePredictingActivityForm.class);
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String text = "Schedule activities with the potential for pleasure, learning, or personal growth. " +
            "Predict how satisfying and rewarding each activity will be from 0% (not at all) to 100% (the most).\n\n" +
            "After you complete each activity, record how satisfying it turned out to be, using the same scale. " +
            "Often you'll discover that many activities are more satisfying than you predicted.\n\n" +
            "You can use the technique to test certain Self-Defeating Beliefs, such as \"I need love to feel happy and worthwhile. " +
            "If I'm alone, I'm bound to be miserable.\"";
        JOptionPane.showMessageDialog(parent, new JScrollPane(Constants.getInfoArea(text)),
            Constants.PLEASURE_TITLE,
            JOptionPane.PLAIN_MESSAGE);
      }
    });
  }

  @Override
  public String toString() {
    return Constants.PLEASURE_TITLE;
  }

  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer
   * >>> IMPORTANT!! <<<
   * DO NOT edit this method OR call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    mainPanel = new JPanel();
    mainPanel.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
    final JLabel label1 = new JLabel();
    Font label1Font = this.$$$getFont$$$(null, Font.BOLD, -1, label1.getFont());
    if (label1Font != null) label1.setFont(label1Font);
    label1.setText("Belief:");
    mainPanel.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    textField1 = new JTextField();
    mainPanel.add(textField1, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    final JLabel label2 = new JLabel();
    Font label2Font = this.$$$getFont$$$(null, Font.ITALIC, -1, label2.getFont());
    if (label2Font != null) label2.setFont(label2Font);
    label2.setText("E.g.: I can't feel truly happy or fulfilled without love. If I'm rejected or alone, I'm bound to feel worthless and miserable.");
    mainPanel.add(label2, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    contentPanel = new JPanel();
    contentPanel.setLayout(new BorderLayout(0, 0));
    mainPanel.add(contentPanel, new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    final JPanel panel1 = new JPanel();
    panel1.setLayout(new BorderLayout(0, 0));
    mainPanel.add(panel1, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JLabel label3 = new JLabel();
    Font label3Font = this.$$$getFont$$$(null, -1, 20, label3.getFont());
    if (label3Font != null) label3.setFont(label3Font);
    label3.setHorizontalAlignment(0);
    label3.setHorizontalTextPosition(0);
    label3.setText("Pleasure-Predicting Sheet");
    panel1.add(label3, BorderLayout.CENTER);
    button1 = new JButton();
    button1.setBorderPainted(false);
    button1.setContentAreaFilled(false);
    button1.setFocusPainted(false);
    button1.setFocusable(false);
    button1.setHorizontalTextPosition(0);
    button1.setIcon(new ImageIcon(getClass().getResource("/help-icon-11-24.png")));
    button1.setMaximumSize(new Dimension(32, 32));
    button1.setMinimumSize(new Dimension(32, 32));
    button1.setOpaque(false);
    button1.setPreferredSize(new Dimension(32, 32));
    button1.setText("");
    button1.setToolTipText("");
    button1.setVerifyInputWhenFocusTarget(false);
    panel1.add(button1, BorderLayout.EAST);
  }

  /**
   * @noinspection ALL
   */
  private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
    if (currentFont == null) return null;
    String resultName;
    if (fontName == null) {
      resultName = currentFont.getName();
    } else {
      Font testFont = new Font(fontName, Font.PLAIN, 10);
      if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
        resultName = fontName;
      } else {
        resultName = currentFont.getName();
      }
    }
    return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
  }

  /**
   * @noinspection ALL
   */
  public JComponent $$$getRootComponent$$$() {
    return mainPanel;
  }

  public PleasurePredictingSheet getData() {
    PleasurePredictingSheet pps = new PleasurePredictingSheet();
    pps.setBelief(textField1.getText().trim());
    pps.setActivities(new TreeMap<>());
    for (int i = 0; i < dnd.getTabCount() - 1; i++) {
      Datable d = dnd.datables.get(i);
      pps.getActivities().put(i, (Activity) d.getData());
    }
    return pps;
  }

  public void setData(PleasurePredictingSheet pleasurePredictingSheet) {
    textField1.setText(pleasurePredictingSheet.getBelief());
    if (pleasurePredictingSheet.getActivities().size() > 0) {
      for (int i = 0; i < dnd.getTabCount() - 1; i++) {
        dnd.removeTabAt(i);
      }
      dnd.datables.clear();
    }
    for (Activity activity : pleasurePredictingSheet.getActivities().values()) {
      PleasurePredictingActivityForm form = new PleasurePredictingActivityForm();
      form.setData(activity);
      dnd.addNewTab(form, PleasurePredictingActivityForm.class);
    }
    if (dnd.getTabCount() > 1) {
      dnd.setSelectedIndex(0);
    }
  }
}
