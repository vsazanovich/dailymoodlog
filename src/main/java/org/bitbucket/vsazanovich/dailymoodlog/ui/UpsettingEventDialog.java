package org.bitbucket.vsazanovich.dailymoodlog.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.UpsettingEvent;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;

import static org.bitbucket.vsazanovich.dailymoodlog.Constants.CURRENT_PROJECT;

public class UpsettingEventDialog extends JDialog {

  private UpsettingEvent thisInstance;
  private JPanel contentPane;
  private JButton buttonOK;
  private JButton buttonCancel;
  private JTextField textField1;
  private JCheckBox cb1;
  private JCheckBox cb2;
  private JCheckBox cb3;
  private JCheckBox cb4;
  private JCheckBox cb5;
  private JCheckBox cb6;
  private JCheckBox cb7;
  private JCheckBox cb8;
  private JCheckBox cb9;
  private JCheckBox cb10;
  private JCheckBox cb11;
  private JCheckBox cb12;
  private JCheckBox cb13;
  private JCheckBox cb14;
  private JCheckBox cb15;
  private JCheckBox cb16;
  private JCheckBox cb17;
  private JCheckBox cb18;
  private JCheckBox cb19;
  private JCheckBox cb20;
  private JCheckBox cb21;
  private JCheckBox cb22;
  private JCheckBox cb23;
  private JCheckBox cb24;
  private JCheckBox cb25;
  private JCheckBox cb26;
  private JCheckBox cb27;
  private JCheckBox cb28;
  private JCheckBox cb29;
  private JCheckBox cb30;
  private JCheckBox cb31;
  private JCheckBox cb32;
  private JCheckBox cb33;
  private JCheckBox cb34;
  private JCheckBox cb35;
  private JCheckBox cb36;
  private JCheckBox cb37;
  private JCheckBox cb38;
  private JCheckBox cb39;
  private JCheckBox cb40;
  private JCheckBox cb41;
  private JCheckBox cb42;
  private JCheckBox cb43;
  private JCheckBox cb44;
  private JTextField textField2;
  private JCheckBox cb45;
  private JCheckBox cb46;
  private JCheckBox cb47;
  private JCheckBox cb48;
  private JCheckBox cb49;
  private JCheckBox cb50;
  private JCheckBox cb51;
  private JCheckBox cb52;
  private JCheckBox cb53;
  private JCheckBox cb54;
  private JCheckBox cb55;
  private JCheckBox cb56;
  private JCheckBox cb57;
  private JCheckBox cb58;
  private JTextArea textArea1;
  private JTextArea textArea2;
  private JSlider slider1;
  private JSlider slider2;
  private JSlider slider3;
  private JLabel beforeLabel;
  private JLabel afterLabel;
  private JLabel beliefLabel;
  private MainFrame parent;

  public UpsettingEventDialog(MainFrame owner) {
    super(owner);
    this.parent = owner;
    setContentPane(contentPane);
    setTitle("Daily Mood Log");
    setModal(true);
    getRootPane().setDefaultButton(buttonOK);

    buttonOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onOK();
      }
    });

//    buttonCancel.addActionListener(new ActionListener() {
//      public void actionPerformed(ActionEvent e) {
//        onCancel();
//      }
//    });
    slider1.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider) e.getSource();
        beforeLabel.setText("" + Constants.roundTo(source.getValue()) + "%");
      }
    });

    slider2.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider) e.getSource();
        afterLabel.setText("" + Constants.roundTo(source.getValue()) + "%");
      }
    });

    slider3.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider) e.getSource();
        beliefLabel.setText("" + Constants.roundTo(source.getValue()) + "%");
      }
    });

    textField1.getDocument().addDocumentListener(new DocumentListener() {
      public void changedUpdate(DocumentEvent e) {
        warn();
      }

      public void removeUpdate(DocumentEvent e) {
        warn();
      }

      public void insertUpdate(DocumentEvent e) {
        warn();
      }

      public void warn() {
        buttonOK.setEnabled(!StringUtils.isEmpty(textField1.getText().trim()));
      }
    });

    buttonOK.setEnabled(false);

    // call onCancel() when cross is clicked
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        onCancel();
      }
    });

    // call onCancel() on ESCAPE
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCancel();
      }
    }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
  }


  private void onOK() {
    UpsettingEvent ue = thisInstance;
    if (ue == null) {
      ue = new UpsettingEvent();
      ue.setTime(System.currentTimeMillis());
    }
    ue.setTitle(textField1.getText().trim());
    ue.setOtherEmotions(textField2.getText().trim());
    ue.setNegativeThoughts(textArea1.getText().trim());
    ue.setPositiveThoughts(textArea2.getText().trim());
    ue.setBefore(slider1.getValue());
    ue.setAfter(slider2.getValue());
    ue.setBelief(slider3.getValue());

    boolean[] emotions = new boolean[44];
    try {
      for (int i = 1; i <= 44; i++) {
        JCheckBox o = (JCheckBox) FieldUtils.readField(this, "cb" + i, true);
        emotions[i - 1] = o.isSelected();
      }
      ue.setEmotions(emotions);
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    boolean[] distortions = new boolean[14];
    try {
      for (int i = 45; i <= 58; i++) {
        JCheckBox o = (JCheckBox) FieldUtils.readField(this, "cb" + i, true);
        distortions[i - 45] = o.isSelected();
      }
      ue.setDistortions(distortions);
    } catch (Exception ex) {
      ex.printStackTrace();
    }


    CURRENT_PROJECT.getEvents().put(ue.getTime(), ue);
    dispose();
    parent.saveAction.saveText();

    if (thisInstance == null) {
      ((DefaultListModel) Constants.FRAME.editorPanel.eventsList.getModel()).insertElementAt(ue, 0);
      Constants.FRAME.editorPanel.eventsList.setSelectedIndex(0);
    }
  }

  private void onCancel() {
    // add your code here if necessary
    dispose();
  }

  public static void main(String[] args) {
    UpsettingEventDialog dialog = new UpsettingEventDialog(null);
    dialog.pack();
    dialog.setVisible(true);
    System.exit(0);
  }

  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer
   * >>> IMPORTANT!! <<<
   * DO NOT edit this method OR call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    contentPane = new JPanel();
    contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
    final JPanel panel1 = new JPanel();
    panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
    final Spacer spacer1 = new Spacer();
    panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    final JPanel panel2 = new JPanel();
    panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
    panel1.add(panel2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    buttonOK = new JButton();
    buttonOK.setText("Save");
    panel2.add(buttonOK, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel3 = new JPanel();
    panel3.setLayout(new BorderLayout(0, 0));
    contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JPanel panel4 = new JPanel();
    panel4.setLayout(new GridLayoutManager(4, 4, new Insets(0, 0, 0, 0), -1, -1));
    panel3.add(panel4, BorderLayout.CENTER);
    final JLabel label1 = new JLabel();
    label1.setText("Upsetting Event");
    panel4.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    textField1 = new JTextField();
    panel4.add(textField1, new GridConstraints(0, 2, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    final JLabel label2 = new JLabel();
    label2.setText("Emotions");
    label2.setVerticalAlignment(0);
    label2.setVerticalTextPosition(0);
    panel4.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_NORTHEAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel5 = new JPanel();
    panel5.setLayout(new GridLayoutManager(9, 7, new Insets(0, 0, 0, 0), -1, -1));
    panel4.add(panel5, new GridConstraints(1, 2, 1, 2, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_NONE, 1, 1, null, null, null, 0, false));
    cb1 = new JCheckBox();
    cb1.setFocusPainted(false);
    Font cb1Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb1.getFont());
    if (cb1Font != null) cb1.setFont(cb1Font);
    cb1.setText("sad");
    panel5.add(cb1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb2 = new JCheckBox();
    cb2.setFocusPainted(false);
    cb2.setText("blue");
    panel5.add(cb2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb3 = new JCheckBox();
    cb3.setFocusPainted(false);
    cb3.setText("depressed");
    panel5.add(cb3, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb4 = new JCheckBox();
    cb4.setFocusPainted(false);
    cb4.setText("down");
    panel5.add(cb4, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb5 = new JCheckBox();
    cb5.setFocusPainted(false);
    cb5.setText("unhappy");
    panel5.add(cb5, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb6 = new JCheckBox();
    cb6.setFocusPainted(false);
    Font cb6Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb6.getFont());
    if (cb6Font != null) cb6.setFont(cb6Font);
    cb6.setText("anxious");
    panel5.add(cb6, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb7 = new JCheckBox();
    cb7.setFocusPainted(false);
    cb7.setText("worried");
    panel5.add(cb7, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb8 = new JCheckBox();
    cb8.setFocusPainted(false);
    cb8.setText("panicky");
    panel5.add(cb8, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb9 = new JCheckBox();
    cb9.setFocusPainted(false);
    cb9.setText("nervous");
    panel5.add(cb9, new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb10 = new JCheckBox();
    cb10.setFocusPainted(false);
    cb10.setText("frightened");
    panel5.add(cb10, new GridConstraints(1, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb11 = new JCheckBox();
    cb11.setFocusPainted(false);
    Font cb11Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb11.getFont());
    if (cb11Font != null) cb11.setFont(cb11Font);
    cb11.setText("guilty");
    panel5.add(cb11, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb12 = new JCheckBox();
    cb12.setFocusPainted(false);
    cb12.setText("remorseful");
    panel5.add(cb12, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb13 = new JCheckBox();
    cb13.setFocusPainted(false);
    cb13.setText("bad");
    panel5.add(cb13, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb14 = new JCheckBox();
    cb14.setFocusPainted(false);
    cb14.setText("ashamed");
    panel5.add(cb14, new GridConstraints(2, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb15 = new JCheckBox();
    cb15.setFocusPainted(false);
    Font cb15Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb15.getFont());
    if (cb15Font != null) cb15.setFont(cb15Font);
    cb15.setText("inferior");
    panel5.add(cb15, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb16 = new JCheckBox();
    cb16.setFocusPainted(false);
    cb16.setText("worthless");
    panel5.add(cb16, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb17 = new JCheckBox();
    cb17.setFocusPainted(false);
    cb17.setText("inadequate");
    panel5.add(cb17, new GridConstraints(3, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb18 = new JCheckBox();
    cb18.setFocusPainted(false);
    cb18.setText("defective");
    panel5.add(cb18, new GridConstraints(3, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb19 = new JCheckBox();
    cb19.setFocusPainted(false);
    cb19.setText("incompetent");
    panel5.add(cb19, new GridConstraints(3, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb20 = new JCheckBox();
    cb20.setFocusPainted(false);
    Font cb20Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb20.getFont());
    if (cb20Font != null) cb20.setFont(cb20Font);
    cb20.setText("lonely");
    panel5.add(cb20, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb21 = new JCheckBox();
    cb21.setFocusPainted(false);
    cb21.setText("unloved");
    panel5.add(cb21, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb22 = new JCheckBox();
    cb22.setFocusPainted(false);
    cb22.setText("unwanted");
    panel5.add(cb22, new GridConstraints(4, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb23 = new JCheckBox();
    cb23.setFocusPainted(false);
    cb23.setText("rejected");
    panel5.add(cb23, new GridConstraints(4, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb24 = new JCheckBox();
    cb24.setFocusPainted(false);
    cb24.setText("alone");
    panel5.add(cb24, new GridConstraints(4, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb25 = new JCheckBox();
    cb25.setFocusPainted(false);
    cb25.setText("abandoned");
    panel5.add(cb25, new GridConstraints(4, 5, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb26 = new JCheckBox();
    cb26.setFocusPainted(false);
    Font cb26Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb26.getFont());
    if (cb26Font != null) cb26.setFont(cb26Font);
    cb26.setText("embarrasssed");
    panel5.add(cb26, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb27 = new JCheckBox();
    cb27.setFocusPainted(false);
    cb27.setText("foolish");
    panel5.add(cb27, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb28 = new JCheckBox();
    cb28.setFocusPainted(false);
    cb28.setText("humilliated");
    panel5.add(cb28, new GridConstraints(5, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb29 = new JCheckBox();
    cb29.setFocusPainted(false);
    cb29.setText("self-conscious");
    panel5.add(cb29, new GridConstraints(5, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb30 = new JCheckBox();
    cb30.setFocusPainted(false);
    Font cb30Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb30.getFont());
    if (cb30Font != null) cb30.setFont(cb30Font);
    cb30.setText("hopeless");
    panel5.add(cb30, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb31 = new JCheckBox();
    cb31.setFocusPainted(false);
    cb31.setText("discouraged");
    panel5.add(cb31, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb32 = new JCheckBox();
    cb32.setFocusPainted(false);
    cb32.setText("pessimistic");
    panel5.add(cb32, new GridConstraints(6, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb33 = new JCheckBox();
    cb33.setFocusPainted(false);
    cb33.setText("dispairing");
    panel5.add(cb33, new GridConstraints(6, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb34 = new JCheckBox();
    cb34.setFocusPainted(false);
    Font cb34Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb34.getFont());
    if (cb34Font != null) cb34.setFont(cb34Font);
    cb34.setText("frustrated");
    panel5.add(cb34, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb35 = new JCheckBox();
    cb35.setFocusPainted(false);
    cb35.setText("stuck");
    panel5.add(cb35, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb36 = new JCheckBox();
    cb36.setFocusPainted(false);
    cb36.setText("thwarted");
    panel5.add(cb36, new GridConstraints(7, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb37 = new JCheckBox();
    cb37.setFocusPainted(false);
    cb37.setText("defeated");
    panel5.add(cb37, new GridConstraints(7, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb38 = new JCheckBox();
    cb38.setFocusPainted(false);
    Font cb38Font = this.$$$getFont$$$(null, Font.BOLD, -1, cb38.getFont());
    if (cb38Font != null) cb38.setFont(cb38Font);
    cb38.setText("angry");
    panel5.add(cb38, new GridConstraints(8, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb39 = new JCheckBox();
    cb39.setFocusPainted(false);
    cb39.setText("mad");
    panel5.add(cb39, new GridConstraints(8, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb40 = new JCheckBox();
    cb40.setFocusPainted(false);
    cb40.setText("resentful");
    panel5.add(cb40, new GridConstraints(8, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb41 = new JCheckBox();
    cb41.setFocusPainted(false);
    cb41.setText("annoyed");
    panel5.add(cb41, new GridConstraints(8, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb42 = new JCheckBox();
    cb42.setFocusPainted(false);
    cb42.setText("irritated");
    panel5.add(cb42, new GridConstraints(8, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb43 = new JCheckBox();
    cb43.setFocusPainted(false);
    cb43.setText("upset");
    panel5.add(cb43, new GridConstraints(8, 5, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb44 = new JCheckBox();
    cb44.setFocusPainted(false);
    cb44.setText("furious");
    panel5.add(cb44, new GridConstraints(8, 6, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label3 = new JLabel();
    label3.setText("Other (describe)");
    panel4.add(label3, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    textField2 = new JTextField();
    panel4.add(textField2, new GridConstraints(2, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    final JLabel label4 = new JLabel();
    label4.setText("Thoughts");
    label4.setVerticalAlignment(0);
    panel4.add(label4, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_NORTHEAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel6 = new JPanel();
    panel6.setLayout(new BorderLayout(0, 0));
    panel4.add(panel6, new GridConstraints(3, 2, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JPanel panel7 = new JPanel();
    panel7.setLayout(new GridLayoutManager(16, 3, new Insets(0, 0, 0, 0), -1, -1));
    panel6.add(panel7, BorderLayout.CENTER);
    final JLabel label5 = new JLabel();
    label5.setText("Negative Thoughts");
    panel7.add(label5, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label6 = new JLabel();
    label6.setText("Distortions");
    panel7.add(label6, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label7 = new JLabel();
    label7.setText("Positive Thoughts");
    panel7.add(label7, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb45 = new JCheckBox();
    cb45.setFocusPainted(false);
    cb45.setText("All-or-Nothing Thinking");
    cb45.setToolTipText("You view things in absolute, black-and-white categories.");
    panel7.add(cb45, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb46 = new JCheckBox();
    cb46.setFocusPainted(false);
    cb46.setText("Overgeneralization");
    cb46.setToolTipText("You view a negative event as a never-ending pattern of defeat. \"This always happens!\"");
    panel7.add(cb46, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb47 = new JCheckBox();
    cb47.setFocusPainted(false);
    cb47.setText("Mental Filter");
    cb47.setToolTipText("You dwell on the negatives and ignore the positives.");
    panel7.add(cb47, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb48 = new JCheckBox();
    cb48.setFocusPainted(false);
    cb48.setText("Discounting the Positive");
    cb48.setToolTipText("You insist that your positive qualities don't count.");
    panel7.add(cb48, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb49 = new JCheckBox();
    cb49.setFocusPainted(false);
    cb49.setText("Jumping to Conclusions");
    cb49.setToolTipText("You jump to conclustions not warranted by the facts.");
    panel7.add(cb49, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb50 = new JCheckBox();
    cb50.setFocusPainted(false);
    cb50.setText("Mind-Reading");
    cb50.setToolTipText("You assume that people are reacting negatively to you.");
    panel7.add(cb50, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 2, false));
    cb51 = new JCheckBox();
    cb51.setFocusPainted(false);
    cb51.setText("Fortune-Telling");
    cb51.setToolTipText("You predict that things will turn out badly.");
    panel7.add(cb51, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 2, false));
    cb52 = new JCheckBox();
    cb52.setFocusPainted(false);
    cb52.setText("Magnification or Minimization");
    cb52.setToolTipText("You blow things out of proportion or shrink them.");
    panel7.add(cb52, new GridConstraints(8, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb53 = new JCheckBox();
    cb53.setFocusPainted(false);
    cb53.setFocusable(true);
    cb53.setText("Emotional Reasoning");
    cb53.setToolTipText("You reason from your feelings: \"I feel like an idiot, so I must really be one\".");
    panel7.add(cb53, new GridConstraints(9, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb54 = new JCheckBox();
    cb54.setFocusPainted(false);
    cb54.setFocusable(true);
    cb54.setText("Should Statements");
    cb54.setToolTipText("You use shoulds, shouldn'ts, musts, oughts, and have tos.");
    panel7.add(cb54, new GridConstraints(10, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb55 = new JCheckBox();
    cb55.setFocusPainted(false);
    cb55.setFocusable(true);
    cb55.setText("Labelling");
    cb55.setToolTipText("Instead of saying \"I made a mistake\", you say: \"I'm a jerk\", or \"I'm a loser\".");
    panel7.add(cb55, new GridConstraints(11, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb56 = new JCheckBox();
    cb56.setFocusPainted(false);
    cb56.setFocusable(true);
    cb56.setText("Blame");
    cb56.setToolTipText("You find fault instead of solving the problem.");
    panel7.add(cb56, new GridConstraints(12, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    cb57 = new JCheckBox();
    cb57.setFocusPainted(false);
    cb57.setFocusable(true);
    cb57.setText("Self-Blame");
    cb57.setToolTipText("You blame yourself for something you weren't entirely responsible for.");
    panel7.add(cb57, new GridConstraints(13, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 2, false));
    cb58 = new JCheckBox();
    cb58.setFocusPainted(false);
    cb58.setFocusable(true);
    cb58.setText("Other-Blame");
    cb58.setToolTipText("You blame others and overlook ways you contributed to the problem.");
    panel7.add(cb58, new GridConstraints(14, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 2, false));
    final JScrollPane scrollPane1 = new JScrollPane();
    panel7.add(scrollPane1, new GridConstraints(1, 0, 14, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea1 = new JTextArea();
    textArea1.setLineWrap(true);
    textArea1.setMargin(new Insets(5, 5, 5, 5));
    textArea1.setWrapStyleWord(true);
    scrollPane1.setViewportView(textArea1);
    final JScrollPane scrollPane2 = new JScrollPane();
    panel7.add(scrollPane2, new GridConstraints(1, 2, 14, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea2 = new JTextArea();
    textArea2.setLineWrap(true);
    textArea2.setMargin(new Insets(5, 5, 5, 5));
    textArea2.setWrapStyleWord(true);
    scrollPane2.setViewportView(textArea2);
    final JPanel panel8 = new JPanel();
    panel8.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
    panel7.add(panel8, new GridConstraints(15, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JLabel label8 = new JLabel();
    label8.setText("Before");
    panel8.add(label8, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    slider1 = new JSlider();
    slider1.setFocusable(true);
    slider1.setPaintTrack(true);
    panel8.add(slider1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label9 = new JLabel();
    label9.setText("After");
    panel8.add(label9, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    slider2 = new JSlider();
    slider2.setFocusable(true);
    panel8.add(slider2, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    beforeLabel = new JLabel();
    beforeLabel.setText("50%");
    panel8.add(beforeLabel, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, -1), null, null, 0, false));
    afterLabel = new JLabel();
    afterLabel.setText("50%");
    panel8.add(afterLabel, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, -1), null, null, 0, false));
    final JPanel panel9 = new JPanel();
    panel9.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
    panel7.add(panel9, new GridConstraints(15, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JPanel panel10 = new JPanel();
    panel10.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
    panel9.add(panel10, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JLabel label10 = new JLabel();
    label10.setText("Belief");
    panel10.add(label10, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    slider3 = new JSlider();
    slider3.setFocusable(true);
    panel10.add(slider3, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    beliefLabel = new JLabel();
    beliefLabel.setText("50%");
    panel10.add(beliefLabel, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, -1), null, null, 0, false));
    final Spacer spacer2 = new Spacer();
    panel9.add(spacer2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    final JLabel label11 = new JLabel();
    label11.setForeground(new Color(-4521963));
    label11.setText("*");
    panel4.add(label11, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
  }

  /**
   * @noinspection ALL
   */
  private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
    if (currentFont == null) return null;
    String resultName;
    if (fontName == null) {
      resultName = currentFont.getName();
    } else {
      Font testFont = new Font(fontName, Font.PLAIN, 10);
      if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
        resultName = fontName;
      } else {
        resultName = currentFont.getName();
      }
    }
    return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
  }

  /**
   * @noinspection ALL
   */
  public JComponent $$$getRootComponent$$$() {
    return contentPane;
  }

  public void setData(UpsettingEvent sp) {
    try {
      thisInstance = sp;

      textField1.setText(sp.getTitle());
      textField2.setText(sp.getOtherEmotions());
      textArea1.setText(sp.getNegativeThoughts());
      textArea2.setText(sp.getPositiveThoughts());
      slider1.setValue(sp.getBefore());
      slider2.setValue(sp.getAfter());
      slider3.setValue(sp.getBelief());

      for (int i = 1; i <= 44; i++) {
        boolean val = false;
        if (sp.getEmotions() != null && sp.getEmotions().length > 0) {
          val = Boolean.valueOf(sp.getEmotions()[i - 1]);
        }
        JCheckBox o = (JCheckBox) FieldUtils.readField(this, "cb" + i, true);
        o.setSelected(val);
      }
      for (int i = 45; i <= 58; i++) {
        boolean val = false;
        if (sp.getDistortions() != null && sp.getDistortions().length > 0) {
          val = Boolean.valueOf(sp.getDistortions()[i - 45]);
        }
        JCheckBox o = (JCheckBox) FieldUtils.readField(this, "cb" + i, true);
        o.setSelected(val);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
