package org.bitbucket.vsazanovich.dailymoodlog.ui;


import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.BriefMoodSurvey;
import org.bitbucket.vsazanovich.dailymoodlog.model.DMLProject;
import org.bitbucket.vsazanovich.dailymoodlog.model.NegativeThought;
import org.bitbucket.vsazanovich.dailymoodlog.model.UpsettingEvent;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeTableXYDataset;
import org.jfree.ui.RectangleInsets;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 05-09-2019
 */
public class EditorPanel {
  protected MainFrame parentFrame;
  private JPanel mainPanel;
  public JTabbedPane tabbedPane1;
  public JList surveysList;
  private JButton addMoodSurveyButton;
  private JButton button3;
  public JList eventsList;
  private JButton button4;
  public JList thoughtsList;
  public JSplitPane surveysSplitPane;
  private JPanel chartPanel;
  private JButton button1;


  public EditorPanel(MainFrame pf) {
    this.parentFrame = pf;


    $$$getRootComponent$$$().setName(Constants.PROP_EDITOR_COMPONENT);


    try {
      int split = Constants.PROPS.containsKey(Constants.PROP_DIVIDER_LOCATION) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_DIVIDER_LOCATION)) : 200;
      surveysSplitPane.setDividerLocation(split);
    } catch (Exception ex) {
      surveysSplitPane.setDividerLocation(200);
    }

    try {
      int tab = Constants.PROPS.containsKey(Constants.PROP_SELECTED_TAB) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_SELECTED_TAB)) : 0;
      tabbedPane1.setSelectedIndex(tab);
    } catch (Exception ex) {
      tabbedPane1.setSelectedIndex(0);
    }

    //add
    addMoodSurveyButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BriefMoodSurveyDialog dialog = new BriefMoodSurveyDialog(parentFrame);
        dialog.pack();
        dialog.setLocationRelativeTo(parentFrame);
        dialog.setVisible(true);
      }
    });

    button3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        UpsettingEventDialog dialog = new UpsettingEventDialog(parentFrame);
        dialog.pack();
        dialog.setLocationRelativeTo(parentFrame);
        dialog.setVisible(true);
      }
    });

    button4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        CognitiveBehaviorTherapyDialog dialog = new CognitiveBehaviorTherapyDialog(parentFrame);
        dialog.pack();
        dialog.setLocationRelativeTo(parentFrame);
        dialog.setVisible(true);
      }
    });

    //edit on double-click
    surveysList.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent evt) {
        JList list = (JList) evt.getSource();
        if (evt.getClickCount() == 2) {
          // Double-click detected
          int index = list.locationToIndex(evt.getPoint());
          BriefMoodSurvey sp = (BriefMoodSurvey) (surveysList.getModel()).getElementAt(index);
          BriefMoodSurveyDialog dialog = new BriefMoodSurveyDialog(parentFrame);
          dialog.setData(sp);
          dialog.pack();
          dialog.setLocationRelativeTo(parentFrame);
          dialog.setVisible(true);
        }
      }
    });

    //edit on double-click
    eventsList.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent evt) {
        JList list = (JList) evt.getSource();
        if (evt.getClickCount() == 2) {
          // Double-click detected
          int index = list.locationToIndex(evt.getPoint());
          UpsettingEvent sp = (UpsettingEvent) (eventsList.getModel()).getElementAt(index);
          UpsettingEventDialog dialog = new UpsettingEventDialog(parentFrame);
          dialog.setData(sp);
          dialog.pack();
          dialog.setLocationRelativeTo(parentFrame);
          dialog.setVisible(true);
        }
      }
    });

    //edit on double-click
    thoughtsList.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent evt) {
        JList list = (JList) evt.getSource();
        if (evt.getClickCount() == 2) {
          // Double-click detected
          int index = list.locationToIndex(evt.getPoint());
          NegativeThought sp = (NegativeThought) (thoughtsList.getModel()).getElementAt(index);
          CognitiveBehaviorTherapyDialog dialog = new CognitiveBehaviorTherapyDialog(parentFrame);
          dialog.setData(sp);
          dialog.pack();
          dialog.setLocationRelativeTo(parentFrame);
          dialog.setVisible(true);
        }
      }
    });

    //remove
    surveysList.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        int clicked = surveysList.locationToIndex(e.getPoint());
        if (SwingUtilities.isRightMouseButton(e) && clicked == surveysList.getSelectedIndex() && (e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK) {
          JPopupMenu menu = new JPopupMenu();
          JMenuItem item = new JMenuItem("Delete");
          item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              if ((e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK) {
                //removing
                BriefMoodSurvey survey = (BriefMoodSurvey) ((DefaultListModel) surveysList.getModel()).elementAt(surveysList.getSelectedIndex());
                Constants.CURRENT_PROJECT.getSurveys().remove(survey.getTime());
                ((DefaultListModel) surveysList.getModel()).remove(surveysList.getSelectedIndex());

                //selecting
                int nextSelectedIndex = clicked - 1;
                if (nextSelectedIndex < 0) {
                  nextSelectedIndex = 0;
                }
                if (surveysList.getModel().getSize() > 0) {
                  surveysList.setSelectedIndex(nextSelectedIndex);
                  surveysList.requestFocus();
                }
                redrawChart();
                parentFrame.revalidate();
                parentFrame.repaint();
              }
            }
          });
          menu.add(item);
          menu.show(e.getComponent(), e.getX(), e.getY());
        }
      }
    });

    //remove
    eventsList.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        int clicked = eventsList.locationToIndex(e.getPoint());
        if (SwingUtilities.isRightMouseButton(e) && clicked == eventsList.getSelectedIndex() && (e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK) {
          JPopupMenu menu = new JPopupMenu();
          JMenuItem item = new JMenuItem("Delete");
          item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              if ((e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK) {
                //removing
                UpsettingEvent event = (UpsettingEvent) ((DefaultListModel) eventsList.getModel()).elementAt(eventsList.getSelectedIndex());
                Constants.CURRENT_PROJECT.getEvents().remove(event.getTime());
                ((DefaultListModel) eventsList.getModel()).remove(eventsList.getSelectedIndex());

                //selecting
                int nextSelectedIndex = clicked - 1;
                if (nextSelectedIndex < 0) {
                  nextSelectedIndex = 0;
                }
                if (eventsList.getModel().getSize() > 0) {
                  eventsList.setSelectedIndex(nextSelectedIndex);
                  eventsList.requestFocus();
                }
                parentFrame.revalidate();
                parentFrame.repaint();
              }
            }
          });
          menu.add(item);
          menu.show(e.getComponent(), e.getX(), e.getY());
        }
      }
    });

    //remove
    thoughtsList.addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        int clicked = thoughtsList.locationToIndex(e.getPoint());
        if (SwingUtilities.isRightMouseButton(e) && clicked == thoughtsList.getSelectedIndex() && (e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK) {
          JPopupMenu menu = new JPopupMenu();
          JMenuItem item = new JMenuItem("Delete");
          item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              if ((e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK) {
                //removing
                NegativeThought event = (NegativeThought) ((DefaultListModel) thoughtsList.getModel()).elementAt(thoughtsList.getSelectedIndex());
                Constants.CURRENT_PROJECT.getThoughts().remove(event.getTime());
                ((DefaultListModel) thoughtsList.getModel()).remove(thoughtsList.getSelectedIndex());

                //selecting
                int nextSelectedIndex = clicked - 1;
                if (nextSelectedIndex < 0) {
                  nextSelectedIndex = 0;
                }
                if (thoughtsList.getModel().getSize() > 0) {
                  thoughtsList.setSelectedIndex(nextSelectedIndex);
                  thoughtsList.requestFocus();
                }
                parentFrame.revalidate();
                parentFrame.repaint();
              }
            }
          });
          menu.add(item);
          menu.show(e.getComponent(), e.getX(), e.getY());
        }
      }
    });

    surveysList.setModel(new DefaultListModel());
    eventsList.setModel(new DefaultListModel());
    thoughtsList.setModel(new DefaultListModel());

    //for testing
//    SwingUtilities.invokeLater(new Runnable() {
//      public void run() {
//        button4.doClick();
//      }
//    });


    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String text = "Ctrl+O opens an existing log file or creates a new one\n" +
            "(for a new log file browse to a folder and type in the name of your log)\n" +
            "\n" +
            "Ctrl+Q closes the currently open log file\n" +
            "\n" +
            "Ctrl+S saves the current changes\n" +
            "(not necessary, changes are saved regularly)\n" +
            "\n" +
            "Ctrl+P opens the preferences view\n" +
            "(press ESC to return)\n" +
            "\n" +
            "ESC closes the currently open dialog\n" +
            "(changes are not saved)\n" +
            "\n" +
            "Double-Click on a list item opens it for editing\n" +
            "\n" +
            "Ctrl+Right Click on a list item shows delete button\n" +
            "(hold the Ctrl key and click on the delete button to remove the selected item)\n" +
            "\n" +
            "Ctrl - hold the Control key to reveal tab removal buttons\n" +
            "(eg. in Pleasure Predicting Technique -> Activities";
        JOptionPane.showMessageDialog(parentFrame, new JScrollPane(Constants.getInfoArea(text)),
            Constants.QUICK_TITLE,
            JOptionPane.PLAIN_MESSAGE);
      }
    });
  }


  public void setData(DMLProject sb) {
    Constants.CURRENT_PROJECT = sb;
    for (BriefMoodSurvey bms : sb.getSurveys().values()) {
      ((DefaultListModel) surveysList.getModel()).addElement(bms);
    }

    for (UpsettingEvent bms : sb.getEvents().values()) {
      ((DefaultListModel) eventsList.getModel()).addElement(bms);
    }

    for (NegativeThought bms : sb.getThoughts().values()) {
      ((DefaultListModel) thoughtsList.getModel()).addElement(bms);
    }
    eventsList.setSelectedIndex(0);
    surveysList.setSelectedIndex(0);
    thoughtsList.setSelectedIndex(0);

    redrawChart();

  }

  public void redrawChart() {
    TimeTableXYDataset timeTableXYDataset = new TimeTableXYDataset();
    for (BriefMoodSurvey bms : Constants.CURRENT_PROJECT.getSurveys().values()) {
      timeTableXYDataset.add(new Day(new Date(bms.getTime())), bms.getResInt(Constants.RANGE1), "anxious feelings");
      timeTableXYDataset.add(new Day(new Date(bms.getTime())), bms.getResInt(Constants.RANGE2), "anxious physical symptoms");
      timeTableXYDataset.add(new Day(new Date(bms.getTime())), bms.getResInt(Constants.RANGE3), "depression");
    }
    JFreeChart chart = ChartFactory.createTimeSeriesChart("", "time", "mood", timeTableXYDataset, true, true, false);
    XYPlot plot = chart.getXYPlot();
    plot.getRangeAxis().setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    chart.setPadding(new RectangleInsets(20.0, 0.0, 20.0, 5.0));
    ChartPanel cp = new ChartPanel(chart);
    chartPanel.removeAll();
    chartPanel.add(cp);
    parentFrame.revalidate();
    parentFrame.repaint();
  }

  public void unload() {
    Constants.CURRENT_PROJECT = null;
    ((DefaultListModel) surveysList.getModel()).clear();
    ((DefaultListModel) eventsList.getModel()).clear();
    ((DefaultListModel) thoughtsList.getModel()).clear();
  }

  private void createUIComponents() {
    // TODO: place custom component creation code here
  }


  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer
   * >>> IMPORTANT!! <<<
   * DO NOT edit this method OR call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout(0, 0));
    tabbedPane1 = new JTabbedPane();
    tabbedPane1.setFocusable(false);
    mainPanel.add(tabbedPane1, BorderLayout.CENTER);
    final JPanel panel1 = new JPanel();
    panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
    tabbedPane1.addTab("Brief Mood Survey", panel1);
    surveysSplitPane = new JSplitPane();
    panel1.add(surveysSplitPane, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(200, 200), null, 0, false));
    final JPanel panel2 = new JPanel();
    panel2.setLayout(new BorderLayout(0, 0));
    surveysSplitPane.setLeftComponent(panel2);
    final JScrollPane scrollPane1 = new JScrollPane();
    panel2.add(scrollPane1, BorderLayout.CENTER);
    surveysList = new JList();
    surveysList.setFocusable(false);
    scrollPane1.setViewportView(surveysList);
    final JPanel panel3 = new JPanel();
    panel3.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel2.add(panel3, BorderLayout.SOUTH);
    addMoodSurveyButton = new JButton();
    addMoodSurveyButton.setFocusPainted(false);
    addMoodSurveyButton.setText("+");
    panel3.add(addMoodSurveyButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, 30), new Dimension(30, 30), new Dimension(30, 30), 0, false));
    chartPanel = new JPanel();
    chartPanel.setLayout(new BorderLayout(0, 0));
    surveysSplitPane.setRightComponent(chartPanel);
    final JPanel panel4 = new JPanel();
    panel4.setLayout(new BorderLayout(0, 0));
    tabbedPane1.addTab("Daily Mood Log", panel4);
    final JPanel panel5 = new JPanel();
    panel5.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel4.add(panel5, BorderLayout.SOUTH);
    button3 = new JButton();
    button3.setFocusPainted(false);
    button3.setText("+");
    panel5.add(button3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, 30), new Dimension(30, 30), new Dimension(30, 30), 0, false));
    final JScrollPane scrollPane2 = new JScrollPane();
    panel4.add(scrollPane2, BorderLayout.CENTER);
    eventsList = new JList();
    eventsList.setFocusable(false);
    scrollPane2.setViewportView(eventsList);
    final JPanel panel6 = new JPanel();
    panel6.setLayout(new BorderLayout(0, 0));
    tabbedPane1.addTab("Cognitive Behavior Therapy", panel6);
    final JPanel panel7 = new JPanel();
    panel7.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel6.add(panel7, BorderLayout.SOUTH);
    button4 = new JButton();
    button4.setFocusPainted(false);
    button4.setText("+");
    panel7.add(button4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, new Dimension(30, 30), new Dimension(30, 30), new Dimension(30, 30), 0, false));
    final JScrollPane scrollPane3 = new JScrollPane();
    panel6.add(scrollPane3, BorderLayout.CENTER);
    thoughtsList = new JList();
    thoughtsList.setFocusable(false);
    scrollPane3.setViewportView(thoughtsList);
    final JPanel panel8 = new JPanel();
    panel8.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
    mainPanel.add(panel8, BorderLayout.NORTH);
    button1 = new JButton();
    button1.setBorderPainted(false);
    button1.setContentAreaFilled(false);
    button1.setFocusPainted(false);
    button1.setFocusable(false);
    button1.setHorizontalTextPosition(0);
    button1.setIcon(new ImageIcon(getClass().getResource("/help-icon-11-24.png")));
    button1.setMargin(new Insets(3, 3, 3, 3));
    button1.setMaximumSize(new Dimension(32, 32));
    button1.setMinimumSize(new Dimension(32, 32));
    button1.setOpaque(false);
    button1.setPreferredSize(new Dimension(32, 32));
    button1.setText("");
    button1.setToolTipText("");
    button1.setVerifyInputWhenFocusTarget(false);
    panel8.add(button1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final Spacer spacer1 = new Spacer();
    panel8.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
  }

  /**
   * @noinspection ALL
   */
  public JComponent $$$getRootComponent$$$() {
    return mainPanel;
  }

}
