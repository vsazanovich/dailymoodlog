package org.bitbucket.vsazanovich.dailymoodlog.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.apache.commons.lang3.ArrayUtils;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.BriefMoodSurvey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import static org.bitbucket.vsazanovich.dailymoodlog.Constants.CURRENT_PROJECT;

public class BriefMoodSurveyDialog extends JDialog {
  private BriefMoodSurvey thisInstance;

  private JPanel contentPane;
  private JButton buttonSave;
  private JTabbedPane tabbedPane1;
  private JComboBox comboBox1;
  private JComboBox comboBox2;
  private JComboBox comboBox3;
  private JComboBox comboBox4;
  private JComboBox comboBox5;
  private JComboBox comboBox26;
  private JComboBox comboBox27;
  private JComboBox comboBox28;
  private JComboBox comboBox29;
  private JComboBox comboBox210;
  private JLabel totalScoreLabel1;
  private JTextArea textArea1;
  private JTextArea textArea2;
  private JTextArea textArea3;
  private JTextArea textArea4;
  private JComboBox comboBox21;
  private JComboBox comboBox22;
  private JComboBox comboBox23;
  private JComboBox comboBox24;
  private JComboBox comboBox25;
  private JLabel totalScoreLabel2;
  private JLabel totalScoreLabel3;
  private JLabel totalScoreLabel4;
  private JComboBox comboBox31;
  private JComboBox comboBox32;
  private JComboBox comboBox33;
  private JComboBox comboBox34;
  private JComboBox comboBox35;
  private JComboBox comboBox41;
  private JComboBox comboBox42;
  private MainFrame parent;

  public BriefMoodSurveyDialog(MainFrame owner) {
    super(owner);
    this.parent = owner;
    setContentPane(contentPane);
    setTitle("Brief Mood Survey");
    setPreferredSize(new Dimension(600, 800));
    setModal(true);
    getRootPane().setDefaultButton(buttonSave);

    buttonSave.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onOK();
      }
    });

//    buttonCancel.addActionListener(new ActionListener() {
//      public void actionPerformed(ActionEvent e) {
//        onCancel();
//      }
//    });

    // call onCancel() when cross is clicked
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        onCancel();
      }
    });

    // call onCancel() on ESCAPE
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCancel();
      }
    }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    ActionListener al1 = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateTotalScore1();
      }
    };
    ActionListener al2 = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateTotalScore2();
      }
    };

    ActionListener al3 = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateTotalScore3();
      }
    };

    ActionListener al4 = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateTotalScore4();
      }
    };


    comboBox1.addActionListener(al1);
    comboBox2.addActionListener(al1);
    comboBox3.addActionListener(al1);
    comboBox4.addActionListener(al1);
    comboBox5.addActionListener(al1);

    comboBox21.addActionListener(al2);
    comboBox22.addActionListener(al2);
    comboBox23.addActionListener(al2);
    comboBox24.addActionListener(al2);
    comboBox25.addActionListener(al2);
    comboBox26.addActionListener(al2);
    comboBox27.addActionListener(al2);
    comboBox28.addActionListener(al2);
    comboBox29.addActionListener(al2);
    comboBox210.addActionListener(al2);

    comboBox31.addActionListener(al3);
    comboBox32.addActionListener(al3);
    comboBox33.addActionListener(al3);
    comboBox34.addActionListener(al3);
    comboBox35.addActionListener(al3);

    comboBox41.addActionListener(al4);
    comboBox42.addActionListener(al4);
  }

  private void updateTotalScore1() {
    if (comboBox1.getSelectedIndex() == 0 ||
        comboBox2.getSelectedIndex() == 0 ||
        comboBox3.getSelectedIndex() == 0 ||
        comboBox4.getSelectedIndex() == 0 ||
        comboBox5.getSelectedIndex() == 0) {
      totalScoreLabel1.setText("");
      textArea1.setText("");
    } else {
      int totalScore = 0;
      totalScore += comboBox1.getSelectedIndex() - 1;
      totalScore += comboBox2.getSelectedIndex() - 1;
      totalScore += comboBox3.getSelectedIndex() - 1;
      totalScore += comboBox4.getSelectedIndex() - 1;
      totalScore += comboBox5.getSelectedIndex() - 1;

      totalScoreLabel1.setText(String.valueOf(totalScore));

      if (Constants.INTER1MAP.containsKey(totalScore)) {
        textArea1.setText(Constants.INTER1MAP.get(totalScore));
      } else {
        //TODO: log error
        textArea1.setText("");
      }
    }
  }

  private void updateTotalScore2() {
    if (comboBox21.getSelectedIndex() == 0 ||
        comboBox22.getSelectedIndex() == 0 ||
        comboBox23.getSelectedIndex() == 0 ||
        comboBox24.getSelectedIndex() == 0 ||
        comboBox25.getSelectedIndex() == 0 ||
        comboBox26.getSelectedIndex() == 0 ||
        comboBox27.getSelectedIndex() == 0 ||
        comboBox28.getSelectedIndex() == 0 ||
        comboBox29.getSelectedIndex() == 0 ||
        comboBox210.getSelectedIndex() == 0
    ) {
      totalScoreLabel2.setText("");
      textArea2.setText("");
    } else {
      int totalScore = 0;
      totalScore += comboBox21.getSelectedIndex() - 1;
      totalScore += comboBox22.getSelectedIndex() - 1;
      totalScore += comboBox23.getSelectedIndex() - 1;
      totalScore += comboBox24.getSelectedIndex() - 1;
      totalScore += comboBox25.getSelectedIndex() - 1;
      totalScore += comboBox26.getSelectedIndex() - 1;
      totalScore += comboBox27.getSelectedIndex() - 1;
      totalScore += comboBox28.getSelectedIndex() - 1;
      totalScore += comboBox29.getSelectedIndex() - 1;
      totalScore += comboBox210.getSelectedIndex() - 1;

      totalScoreLabel2.setText(String.valueOf(totalScore));

      if (Constants.INTER2MAP.containsKey(totalScore)) {
        textArea2.setText(Constants.INTER2MAP.get(totalScore));
      } else {
        //TODO: log error
        textArea2.setText("");
      }
    }
  }

  private void updateTotalScore3() {
    if (
        comboBox31.getSelectedIndex() == 0 ||
            comboBox32.getSelectedIndex() == 0 ||
            comboBox33.getSelectedIndex() == 0 ||
            comboBox34.getSelectedIndex() == 0 ||
            comboBox35.getSelectedIndex() == 0) {
      totalScoreLabel3.setText("");
      textArea3.setText("");
    } else {
      int totalScore = 0;
      totalScore += comboBox31.getSelectedIndex() - 1;
      totalScore += comboBox32.getSelectedIndex() - 1;
      totalScore += comboBox33.getSelectedIndex() - 1;
      totalScore += comboBox34.getSelectedIndex() - 1;
      totalScore += comboBox35.getSelectedIndex() - 1;

      totalScoreLabel3.setText(String.valueOf(totalScore));

      if (Constants.INTER3MAP.containsKey(totalScore)) {
        textArea3.setText(Constants.INTER3MAP.get(totalScore));
      } else {
        //TODO: log error
        textArea3.setText("");
      }
    }
  }

  private void updateTotalScore4() {
    if (comboBox41.getSelectedIndex() == 0 ||
        comboBox42.getSelectedIndex() == 0) {
      totalScoreLabel4.setText("");
      textArea4.setText("");
    } else {
      int totalScore = 0;
      totalScore += comboBox41.getSelectedIndex() - 1;
      totalScore += comboBox42.getSelectedIndex() - 1;

      totalScoreLabel4.setText(String.valueOf(totalScore));

      if (totalScore == 0) {
        textArea4.setText("");
      } else {
        String res = "";
        if (comboBox41.getSelectedIndex() > 1) {
          res += Constants.INTER41;
        }
        if (comboBox42.getSelectedIndex() > 1) {
          if (res.length() > 0) {
            res += "\n\n\n";
          }
          res += Constants.INTER42;
        }
        textArea4.setText(res);
      }
    }
  }

  private void onOK() {
    BriefMoodSurvey bms = thisInstance;
    if (bms == null) {
      bms = new BriefMoodSurvey();
      bms.setTime(System.currentTimeMillis());
    }
    List<Integer> answers = new ArrayList<>();
    answers.add(comboBox1.getSelectedIndex());
    answers.add(comboBox2.getSelectedIndex());
    answers.add(comboBox3.getSelectedIndex());
    answers.add(comboBox4.getSelectedIndex());
    answers.add(comboBox5.getSelectedIndex());

    answers.add(comboBox21.getSelectedIndex());
    answers.add(comboBox22.getSelectedIndex());
    answers.add(comboBox23.getSelectedIndex());
    answers.add(comboBox24.getSelectedIndex());
    answers.add(comboBox25.getSelectedIndex());
    answers.add(comboBox26.getSelectedIndex());
    answers.add(comboBox27.getSelectedIndex());
    answers.add(comboBox28.getSelectedIndex());
    answers.add(comboBox29.getSelectedIndex());
    answers.add(comboBox210.getSelectedIndex());

    answers.add(comboBox31.getSelectedIndex());
    answers.add(comboBox32.getSelectedIndex());
    answers.add(comboBox33.getSelectedIndex());
    answers.add(comboBox34.getSelectedIndex());
    answers.add(comboBox35.getSelectedIndex());

    answers.add(comboBox41.getSelectedIndex());
    answers.add(comboBox42.getSelectedIndex());

    bms.setAnswers(ArrayUtils.toPrimitive(answers.toArray(new Integer[answers.size()])));
    CURRENT_PROJECT.getSurveys().put(bms.getTime(), bms);
    dispose();

    parent.saveAction.saveText();

    if (thisInstance == null) {
      ((DefaultListModel) Constants.FRAME.editorPanel.surveysList.getModel()).insertElementAt(bms, 0);
      Constants.FRAME.editorPanel.surveysList.setSelectedIndex(0);
    }
    Constants.FRAME.editorPanel.redrawChart();
  }

  public void setData(BriefMoodSurvey sp) {
    thisInstance = sp;

    int counter = 0;
    comboBox1.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox2.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox3.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox4.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox5.setSelectedIndex(sp.getAnswers()[counter++]);

    comboBox21.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox22.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox23.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox24.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox25.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox26.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox27.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox28.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox29.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox210.setSelectedIndex(sp.getAnswers()[counter++]);

    comboBox31.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox32.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox33.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox34.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox35.setSelectedIndex(sp.getAnswers()[counter++]);

    comboBox41.setSelectedIndex(sp.getAnswers()[counter++]);
    comboBox42.setSelectedIndex(sp.getAnswers()[counter++]);

    updateTotalScore1();
    updateTotalScore2();
    updateTotalScore3();
    updateTotalScore4();
  }

  private void onCancel() {
    // add your code here if necessary
    dispose();
  }

  public static void main(String[] args) {
    BriefMoodSurveyDialog dialog = new BriefMoodSurveyDialog(null);
    dialog.setSize(600, 800);
    dialog.setVisible(true);
  }

  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer
   * >>> IMPORTANT!! <<<
   * DO NOT edit this method OR call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    contentPane = new JPanel();
    contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
    final JPanel panel1 = new JPanel();
    panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
    final JPanel panel2 = new JPanel();
    panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
    panel1.add(panel2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    buttonSave = new JButton();
    buttonSave.setText("Save");
    panel2.add(buttonSave, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final Spacer spacer1 = new Spacer();
    panel2.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    final JPanel panel3 = new JPanel();
    panel3.setLayout(new BorderLayout(0, 0));
    contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    tabbedPane1 = new JTabbedPane();
    tabbedPane1.setEnabled(true);
    tabbedPane1.setFocusable(false);
    panel3.add(tabbedPane1, BorderLayout.CENTER);
    final JPanel panel4 = new JPanel();
    panel4.setLayout(new BorderLayout(0, 0));
    tabbedPane1.addTab("Anxious Feelings", panel4);
    final JPanel panel5 = new JPanel();
    panel5.setLayout(new GridLayoutManager(7, 2, new Insets(5, 5, 30, 5), -1, -1));
    panel4.add(panel5, BorderLayout.NORTH);
    final JLabel label1 = new JLabel();
    label1.setText("Anxious");
    panel5.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label2 = new JLabel();
    label2.setText("Nervous");
    panel5.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label3 = new JLabel();
    label3.setText("Worried");
    panel5.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label4 = new JLabel();
    label4.setText("Frightened or apprehensive");
    panel5.add(label4, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label5 = new JLabel();
    label5.setText("Tense or on edge");
    panel5.add(label5, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox1 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
    defaultComboBoxModel1.addElement("----------------------");
    defaultComboBoxModel1.addElement("0 = Not at all");
    defaultComboBoxModel1.addElement("1 = Somewhat");
    defaultComboBoxModel1.addElement("2 = Moderately");
    defaultComboBoxModel1.addElement("3 = A lot");
    defaultComboBoxModel1.addElement("4 = Extremely");
    comboBox1.setModel(defaultComboBoxModel1);
    panel5.add(comboBox1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox2 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
    defaultComboBoxModel2.addElement("----------------------");
    defaultComboBoxModel2.addElement("0 = Not at all");
    defaultComboBoxModel2.addElement("1 = Somewhat");
    defaultComboBoxModel2.addElement("2 = Moderately");
    defaultComboBoxModel2.addElement("3 = A lot");
    defaultComboBoxModel2.addElement("4 = Extremely");
    comboBox2.setModel(defaultComboBoxModel2);
    panel5.add(comboBox2, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox3 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel3 = new DefaultComboBoxModel();
    defaultComboBoxModel3.addElement("----------------------");
    defaultComboBoxModel3.addElement("0 = Not at all");
    defaultComboBoxModel3.addElement("1 = Somewhat");
    defaultComboBoxModel3.addElement("2 = Moderately");
    defaultComboBoxModel3.addElement("3 = A lot");
    defaultComboBoxModel3.addElement("4 = Extremely");
    comboBox3.setModel(defaultComboBoxModel3);
    panel5.add(comboBox3, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox4 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel4 = new DefaultComboBoxModel();
    defaultComboBoxModel4.addElement("----------------------");
    defaultComboBoxModel4.addElement("0 = Not at all");
    defaultComboBoxModel4.addElement("1 = Somewhat");
    defaultComboBoxModel4.addElement("2 = Moderately");
    defaultComboBoxModel4.addElement("3 = A lot");
    defaultComboBoxModel4.addElement("4 = Extremely");
    comboBox4.setModel(defaultComboBoxModel4);
    panel5.add(comboBox4, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox5 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel5 = new DefaultComboBoxModel();
    defaultComboBoxModel5.addElement("----------------------");
    defaultComboBoxModel5.addElement("0 = Not at all");
    defaultComboBoxModel5.addElement("1 = Somewhat");
    defaultComboBoxModel5.addElement("2 = Moderately");
    defaultComboBoxModel5.addElement("3 = A lot");
    defaultComboBoxModel5.addElement("4 = Extremely");
    comboBox5.setModel(defaultComboBoxModel5);
    panel5.add(comboBox5, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label6 = new JLabel();
    Font label6Font = this.$$$getFont$$$(null, Font.BOLD, -1, label6.getFont());
    if (label6Font != null) label6.setFont(label6Font);
    label6.setText("TOTAL:");
    panel5.add(label6, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    totalScoreLabel1 = new JLabel();
    totalScoreLabel1.setText("");
    panel5.add(totalScoreLabel1, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label7 = new JLabel();
    label7.setText("    ");
    panel5.add(label7, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel6 = new JPanel();
    panel6.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel4.add(panel6, BorderLayout.CENTER);
    panel6.setBorder(BorderFactory.createTitledBorder("Interpretation of the total score"));
    final JScrollPane scrollPane1 = new JScrollPane();
    panel6.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea1 = new JTextArea();
    textArea1.setEditable(false);
    textArea1.setLineWrap(true);
    textArea1.setWrapStyleWord(true);
    scrollPane1.setViewportView(textArea1);
    final JPanel panel7 = new JPanel();
    panel7.setLayout(new BorderLayout(0, 0));
    tabbedPane1.addTab("Anxious Physical Symptoms", panel7);
    final JPanel panel8 = new JPanel();
    panel8.setLayout(new GridLayoutManager(12, 6, new Insets(5, 5, 30, 5), -1, -1));
    panel7.add(panel8, BorderLayout.NORTH);
    final JLabel label8 = new JLabel();
    label8.setText("Skipping, racing, or pounding of the heart");
    panel8.add(label8, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label9 = new JLabel();
    label9.setText("Sweating, chills, or hot flushes");
    panel8.add(label9, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label10 = new JLabel();
    label10.setText("Trembling or shaking");
    panel8.add(label10, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label11 = new JLabel();
    label11.setText("Feeling short of breath or difficulty breathing");
    panel8.add(label11, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label12 = new JLabel();
    label12.setText("Feeling like you're choking");
    panel8.add(label12, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox21 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel6 = new DefaultComboBoxModel();
    defaultComboBoxModel6.addElement("----------------------");
    defaultComboBoxModel6.addElement("0 = Not at all");
    defaultComboBoxModel6.addElement("1 = Somewhat");
    defaultComboBoxModel6.addElement("2 = Moderately");
    defaultComboBoxModel6.addElement("3 = A lot");
    defaultComboBoxModel6.addElement("4 = Extremely");
    comboBox21.setModel(defaultComboBoxModel6);
    panel8.add(comboBox21, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox22 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel7 = new DefaultComboBoxModel();
    defaultComboBoxModel7.addElement("----------------------");
    defaultComboBoxModel7.addElement("0 = Not at all");
    defaultComboBoxModel7.addElement("1 = Somewhat");
    defaultComboBoxModel7.addElement("2 = Moderately");
    defaultComboBoxModel7.addElement("3 = A lot");
    defaultComboBoxModel7.addElement("4 = Extremely");
    comboBox22.setModel(defaultComboBoxModel7);
    panel8.add(comboBox22, new GridConstraints(1, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox23 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel8 = new DefaultComboBoxModel();
    defaultComboBoxModel8.addElement("----------------------");
    defaultComboBoxModel8.addElement("0 = Not at all");
    defaultComboBoxModel8.addElement("1 = Somewhat");
    defaultComboBoxModel8.addElement("2 = Moderately");
    defaultComboBoxModel8.addElement("3 = A lot");
    defaultComboBoxModel8.addElement("4 = Extremely");
    comboBox23.setModel(defaultComboBoxModel8);
    panel8.add(comboBox23, new GridConstraints(2, 1, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox24 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel9 = new DefaultComboBoxModel();
    defaultComboBoxModel9.addElement("----------------------");
    defaultComboBoxModel9.addElement("0 = Not at all");
    defaultComboBoxModel9.addElement("1 = Somewhat");
    defaultComboBoxModel9.addElement("2 = Moderately");
    defaultComboBoxModel9.addElement("3 = A lot");
    defaultComboBoxModel9.addElement("4 = Extremely");
    comboBox24.setModel(defaultComboBoxModel9);
    panel8.add(comboBox24, new GridConstraints(3, 1, 1, 4, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox25 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel10 = new DefaultComboBoxModel();
    defaultComboBoxModel10.addElement("----------------------");
    defaultComboBoxModel10.addElement("0 = Not at all");
    defaultComboBoxModel10.addElement("1 = Somewhat");
    defaultComboBoxModel10.addElement("2 = Moderately");
    defaultComboBoxModel10.addElement("3 = A lot");
    defaultComboBoxModel10.addElement("4 = Extremely");
    comboBox25.setModel(defaultComboBoxModel10);
    panel8.add(comboBox25, new GridConstraints(4, 1, 1, 5, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox26 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel11 = new DefaultComboBoxModel();
    defaultComboBoxModel11.addElement("----------------------");
    defaultComboBoxModel11.addElement("0 = Not at all");
    defaultComboBoxModel11.addElement("1 = Somewhat");
    defaultComboBoxModel11.addElement("2 = Moderately");
    defaultComboBoxModel11.addElement("3 = A lot");
    defaultComboBoxModel11.addElement("4 = Extremely");
    comboBox26.setModel(defaultComboBoxModel11);
    panel8.add(comboBox26, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label13 = new JLabel();
    label13.setText("Pain or tightness in the chest");
    panel8.add(label13, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox27 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel12 = new DefaultComboBoxModel();
    defaultComboBoxModel12.addElement("----------------------");
    defaultComboBoxModel12.addElement("0 = Not at all");
    defaultComboBoxModel12.addElement("1 = Somewhat");
    defaultComboBoxModel12.addElement("2 = Moderately");
    defaultComboBoxModel12.addElement("3 = A lot");
    defaultComboBoxModel12.addElement("4 = Extremely");
    comboBox27.setModel(defaultComboBoxModel12);
    panel8.add(comboBox27, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label14 = new JLabel();
    label14.setText("Butterflies, nausea, or upset stomach");
    panel8.add(label14, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox28 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel13 = new DefaultComboBoxModel();
    defaultComboBoxModel13.addElement("----------------------");
    defaultComboBoxModel13.addElement("0 = Not at all");
    defaultComboBoxModel13.addElement("1 = Somewhat");
    defaultComboBoxModel13.addElement("2 = Moderately");
    defaultComboBoxModel13.addElement("3 = A lot");
    defaultComboBoxModel13.addElement("4 = Extremely");
    comboBox28.setModel(defaultComboBoxModel13);
    panel8.add(comboBox28, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label15 = new JLabel();
    label15.setText("Feeling dizzy, light-headed, or off-balance");
    panel8.add(label15, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox29 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel14 = new DefaultComboBoxModel();
    defaultComboBoxModel14.addElement("----------------------");
    defaultComboBoxModel14.addElement("0 = Not at all");
    defaultComboBoxModel14.addElement("1 = Somewhat");
    defaultComboBoxModel14.addElement("2 = Moderately");
    defaultComboBoxModel14.addElement("3 = A lot");
    defaultComboBoxModel14.addElement("4 = Extremely");
    comboBox29.setModel(defaultComboBoxModel14);
    panel8.add(comboBox29, new GridConstraints(8, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label16 = new JLabel();
    label16.setText("Feeling like you're unreal or the world is unreal");
    panel8.add(label16, new GridConstraints(8, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox210 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel15 = new DefaultComboBoxModel();
    defaultComboBoxModel15.addElement("----------------------");
    defaultComboBoxModel15.addElement("0 = Not at all");
    defaultComboBoxModel15.addElement("1 = Somewhat");
    defaultComboBoxModel15.addElement("2 = Moderately");
    defaultComboBoxModel15.addElement("3 = A lot");
    defaultComboBoxModel15.addElement("4 = Extremely");
    comboBox210.setModel(defaultComboBoxModel15);
    panel8.add(comboBox210, new GridConstraints(9, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label17 = new JLabel();
    label17.setText("Numbness or tingling sensations");
    panel8.add(label17, new GridConstraints(9, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label18 = new JLabel();
    label18.setText("    ");
    panel8.add(label18, new GridConstraints(10, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label19 = new JLabel();
    Font label19Font = this.$$$getFont$$$(null, Font.BOLD, -1, label19.getFont());
    if (label19Font != null) label19.setFont(label19Font);
    label19.setText("TOTAL:");
    panel8.add(label19, new GridConstraints(11, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    totalScoreLabel2 = new JLabel();
    totalScoreLabel2.setText("");
    panel8.add(totalScoreLabel2, new GridConstraints(11, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel9 = new JPanel();
    panel9.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel7.add(panel9, BorderLayout.CENTER);
    panel9.setBorder(BorderFactory.createTitledBorder("Interpretation of the total score"));
    final JScrollPane scrollPane2 = new JScrollPane();
    panel9.add(scrollPane2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea2 = new JTextArea();
    textArea2.setEditable(false);
    textArea2.setFocusable(false);
    textArea2.setLineWrap(true);
    textArea2.setWrapStyleWord(true);
    scrollPane2.setViewportView(textArea2);
    final JPanel panel10 = new JPanel();
    panel10.setLayout(new BorderLayout(0, 0));
    tabbedPane1.addTab("Depression", panel10);
    final JPanel panel11 = new JPanel();
    panel11.setLayout(new GridLayoutManager(7, 2, new Insets(5, 5, 30, 5), -1, -1));
    panel10.add(panel11, BorderLayout.NORTH);
    final JLabel label20 = new JLabel();
    label20.setText("Sad or down in the dumps");
    panel11.add(label20, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label21 = new JLabel();
    label21.setText("Discouraged or hopeless");
    panel11.add(label21, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label22 = new JLabel();
    label22.setText("Low self-esteem");
    panel11.add(label22, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label23 = new JLabel();
    label23.setText("Worthless or inadequate");
    panel11.add(label23, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label24 = new JLabel();
    label24.setText("Loss of pleasure or satisfaction in life");
    panel11.add(label24, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox31 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel16 = new DefaultComboBoxModel();
    defaultComboBoxModel16.addElement("----------------------");
    defaultComboBoxModel16.addElement("0 = Not at all");
    defaultComboBoxModel16.addElement("1 = Somewhat");
    defaultComboBoxModel16.addElement("2 = Moderately");
    defaultComboBoxModel16.addElement("3 = A lot");
    defaultComboBoxModel16.addElement("4 = Extremely");
    comboBox31.setModel(defaultComboBoxModel16);
    panel11.add(comboBox31, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox32 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel17 = new DefaultComboBoxModel();
    defaultComboBoxModel17.addElement("----------------------");
    defaultComboBoxModel17.addElement("0 = Not at all");
    defaultComboBoxModel17.addElement("1 = Somewhat");
    defaultComboBoxModel17.addElement("2 = Moderately");
    defaultComboBoxModel17.addElement("3 = A lot");
    defaultComboBoxModel17.addElement("4 = Extremely");
    comboBox32.setModel(defaultComboBoxModel17);
    panel11.add(comboBox32, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox33 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel18 = new DefaultComboBoxModel();
    defaultComboBoxModel18.addElement("----------------------");
    defaultComboBoxModel18.addElement("0 = Not at all");
    defaultComboBoxModel18.addElement("1 = Somewhat");
    defaultComboBoxModel18.addElement("2 = Moderately");
    defaultComboBoxModel18.addElement("3 = A lot");
    defaultComboBoxModel18.addElement("4 = Extremely");
    comboBox33.setModel(defaultComboBoxModel18);
    panel11.add(comboBox33, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox34 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel19 = new DefaultComboBoxModel();
    defaultComboBoxModel19.addElement("----------------------");
    defaultComboBoxModel19.addElement("0 = Not at all");
    defaultComboBoxModel19.addElement("1 = Somewhat");
    defaultComboBoxModel19.addElement("2 = Moderately");
    defaultComboBoxModel19.addElement("3 = A lot");
    defaultComboBoxModel19.addElement("4 = Extremely");
    comboBox34.setModel(defaultComboBoxModel19);
    panel11.add(comboBox34, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox35 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel20 = new DefaultComboBoxModel();
    defaultComboBoxModel20.addElement("----------------------");
    defaultComboBoxModel20.addElement("0 = Not at all");
    defaultComboBoxModel20.addElement("1 = Somewhat");
    defaultComboBoxModel20.addElement("2 = Moderately");
    defaultComboBoxModel20.addElement("3 = A lot");
    defaultComboBoxModel20.addElement("4 = Extremely");
    comboBox35.setModel(defaultComboBoxModel20);
    panel11.add(comboBox35, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label25 = new JLabel();
    label25.setText("    ");
    panel11.add(label25, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label26 = new JLabel();
    Font label26Font = this.$$$getFont$$$(null, Font.BOLD, -1, label26.getFont());
    if (label26Font != null) label26.setFont(label26Font);
    label26.setText("TOTAL:");
    panel11.add(label26, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    totalScoreLabel3 = new JLabel();
    totalScoreLabel3.setText("");
    panel11.add(totalScoreLabel3, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel12 = new JPanel();
    panel12.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel10.add(panel12, BorderLayout.CENTER);
    panel12.setBorder(BorderFactory.createTitledBorder("Interpretation of the total score"));
    final JScrollPane scrollPane3 = new JScrollPane();
    panel12.add(scrollPane3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea3 = new JTextArea();
    textArea3.setEditable(false);
    textArea3.setFocusable(false);
    textArea3.setLineWrap(true);
    textArea3.setWrapStyleWord(true);
    scrollPane3.setViewportView(textArea3);
    final JPanel panel13 = new JPanel();
    panel13.setLayout(new BorderLayout(0, 0));
    tabbedPane1.addTab("Suicidal Urges", panel13);
    final JPanel panel14 = new JPanel();
    panel14.setLayout(new GridLayoutManager(4, 3, new Insets(5, 5, 30, 5), -1, -1));
    panel13.add(panel14, BorderLayout.NORTH);
    final JLabel label27 = new JLabel();
    label27.setText("Do you have any suicidal thoughts?");
    panel14.add(label27, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label28 = new JLabel();
    label28.setText("Would you like to end your life?");
    panel14.add(label28, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox41 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel21 = new DefaultComboBoxModel();
    defaultComboBoxModel21.addElement("----------------------");
    defaultComboBoxModel21.addElement("0 = Not at all");
    defaultComboBoxModel21.addElement("1 = Somewhat");
    defaultComboBoxModel21.addElement("2 = Moderately");
    defaultComboBoxModel21.addElement("3 = A lot");
    defaultComboBoxModel21.addElement("4 = Extremely");
    comboBox41.setModel(defaultComboBoxModel21);
    panel14.add(comboBox41, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    comboBox42 = new JComboBox();
    final DefaultComboBoxModel defaultComboBoxModel22 = new DefaultComboBoxModel();
    defaultComboBoxModel22.addElement("----------------------");
    defaultComboBoxModel22.addElement("0 = Not at all");
    defaultComboBoxModel22.addElement("1 = Somewhat");
    defaultComboBoxModel22.addElement("2 = Moderately");
    defaultComboBoxModel22.addElement("3 = A lot");
    defaultComboBoxModel22.addElement("4 = Extremely");
    comboBox42.setModel(defaultComboBoxModel22);
    panel14.add(comboBox42, new GridConstraints(1, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final Spacer spacer2 = new Spacer();
    panel14.add(spacer2, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    final JLabel label29 = new JLabel();
    label29.setText("   ");
    panel14.add(label29, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label30 = new JLabel();
    Font label30Font = this.$$$getFont$$$(null, Font.BOLD, -1, label30.getFont());
    if (label30Font != null) label30.setFont(label30Font);
    label30.setText("TOTAL:");
    panel14.add(label30, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    totalScoreLabel4 = new JLabel();
    totalScoreLabel4.setText("");
    panel14.add(totalScoreLabel4, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel15 = new JPanel();
    panel15.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel13.add(panel15, BorderLayout.CENTER);
    panel15.setBorder(BorderFactory.createTitledBorder("Interpretation of the total score"));
    final JScrollPane scrollPane4 = new JScrollPane();
    panel15.add(scrollPane4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea4 = new JTextArea();
    textArea4.setEditable(false);
    textArea4.setFocusable(false);
    textArea4.setLineWrap(true);
    textArea4.setWrapStyleWord(true);
    scrollPane4.setViewportView(textArea4);
  }

  /**
   * @noinspection ALL
   */
  private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
    if (currentFont == null) return null;
    String resultName;
    if (fontName == null) {
      resultName = currentFont.getName();
    } else {
      Font testFont = new Font(fontName, Font.PLAIN, 10);
      if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
        resultName = fontName;
      } else {
        resultName = currentFont.getName();
      }
    }
    return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
  }

  /**
   * @noinspection ALL
   */
  public JComponent $$$getRootComponent$$$() {
    return contentPane;
  }

}
