package org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.uncovering;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.DownwardArrow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 18-10-2019
 */
public class DownwardArrowForm {

  public JPanel mainPanel;
  private JRadioButton rb1;
  private JRadioButton rb2;
  private JRadioButton rb3;
  private JRadioButton rb7;
  private JRadioButton rb8;
  private JRadioButton rb9;
  private JRadioButton rb4;
  private JRadioButton rb5;
  private JRadioButton rb6;
  private JRadioButton rb10;
  private JRadioButton rb11;
  private JRadioButton rb12;
  private JRadioButton rb15;
  private JRadioButton rb16;
  private JRadioButton rb17;
  private JRadioButton rb18;
  private JRadioButton rb19;
  private JRadioButton rb20;
  private JRadioButton rb21;
  private JRadioButton rb13;
  private JRadioButton rb14;
  private JRadioButton rb24;
  private JRadioButton rb23;
  private JRadioButton rb22;
  private JTextField textField1;
  private JTextArea textArea2;
  private JTextArea textArea1;
  private JButton button1;
  private String tmpExplanation = "";
  private JDialog parent;

  public DownwardArrowForm(JDialog d) {
    this.parent = d;
    mainPanel.setName(Constants.DOWNWARD_TITLE);
    ItemListener il = new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        try {
          if (e.getStateChange() == ItemEvent.SELECTED) {
            for (int i = 1; i <= 24; i++) {
              JRadioButton rb = (JRadioButton) FieldUtils.readField(DownwardArrowForm.this, "rb" + i, true);
              if (rb.isSelected()) {
                setState(false);
                textArea2.setText(Constants.SDB_EXPLANATIONS[i - 1]);
                return;
              }
            }

          }
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    };
    ItemListener il2 = new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        try {
          if (e.getStateChange() == ItemEvent.SELECTED) {
            setState(true);
          } else {
            tmpExplanation = textArea2.getText();
          }
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    };
    rb1.addItemListener(il);
    rb2.addItemListener(il);
    rb3.addItemListener(il);
    rb7.addItemListener(il);
    rb8.addItemListener(il);
    rb9.addItemListener(il);
    rb15.addItemListener(il);
    rb16.addItemListener(il);
    rb17.addItemListener(il);
    rb18.addItemListener(il);
    rb19.addItemListener(il);
    rb20.addItemListener(il);
    rb21.addItemListener(il);
    rb4.addItemListener(il);
    rb5.addItemListener(il);
    rb6.addItemListener(il);
    rb10.addItemListener(il);
    rb11.addItemListener(il);
    rb12.addItemListener(il);
    rb13.addItemListener(il);
    rb14.addItemListener(il);
    rb22.addItemListener(il);
    rb23.addItemListener(il);
    rb24.addItemListener(il2);
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String text = "The Downward Arrow Technique will help you identify the Self-Defeating Beliefs that make you vulnerable to depression and anxiety.\n\n" +
            "Draw a downward arrow under a negative thought and ask yourself, " +
            "\"If this thought were true, why would it be upsetting me? What would it mean to me?\"\n\n" +
            "A new negative thought will come to mind. Write it down underneath the arrow and draw another arrow under it. " +
            "Repeat this process several times. \n\n" +
            "Now look at the negative thoughts you generated and review the list of Self-Defeating Beliefs " +
            "so you can pin-point the underlying beliefs at the core of your suffering.";
        JOptionPane.showMessageDialog(d, new JScrollPane(Constants.getInfoArea(text)),
            Constants.DOWNWARD_TITLE,
            JOptionPane.PLAIN_MESSAGE);
      }
    });
  }

  @Override
  public String toString() {
    return Constants.DOWNWARD_TITLE;
  }


  private void setState(boolean state) {
    textField1.setEnabled(state);
    textField1.setEditable(state);
//    textArea2.setEnabled(state);
    textArea2.setEditable(state);
    if (state) {
      textArea2.setText(tmpExplanation);
    } else {
    }
  }

  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer
   * >>> IMPORTANT!! <<<
   * DO NOT edit this method OR call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    final JPanel panel1 = new JPanel();
    panel1.setLayout(new BorderLayout(0, 0));
    mainPanel = new JPanel();
    mainPanel.setLayout(new BorderLayout(0, 0));
    panel1.add(mainPanel, BorderLayout.CENTER);
    final JPanel panel2 = new JPanel();
    panel2.setLayout(new GridLayoutManager(3, 4, new Insets(0, 0, 0, 0), -1, -1));
    mainPanel.add(panel2, BorderLayout.CENTER);
    final JPanel panel3 = new JPanel();
    panel3.setLayout(new BorderLayout(0, 0));
    panel2.add(panel3, new GridConstraints(2, 1, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    panel3.setBorder(BorderFactory.createTitledBorder(""));
    final JPanel panel4 = new JPanel();
    panel4.setLayout(new GridLayoutManager(21, 2, new Insets(5, 5, 5, 5), -1, -1));
    panel3.add(panel4, BorderLayout.CENTER);
    panel4.setBorder(BorderFactory.createTitledBorder(""));
    final JLabel label1 = new JLabel();
    Font label1Font = this.$$$getFont$$$(null, Font.BOLD, -1, label1.getFont());
    if (label1Font != null) label1.setFont(label1Font);
    label1.setText("Achievement");
    panel4.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb1 = new JRadioButton();
    rb1.setFocusPainted(false);
    rb1.setText("Performance Perfectionism");
    panel4.add(rb1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb2 = new JRadioButton();
    rb2.setFocusPainted(false);
    rb2.setText("Perceived Perfectionism");
    panel4.add(rb2, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb3 = new JRadioButton();
    rb3.setFocusPainted(false);
    rb3.setText("Achievement Addiction");
    panel4.add(rb3, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb24 = new JRadioButton();
    rb24.setFocusPainted(false);
    rb24.setText("Your own:");
    panel4.add(rb24, new GridConstraints(17, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel5 = new JPanel();
    panel5.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel4.add(panel5, new GridConstraints(18, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, 1, 1, null, null, null, 0, false));
    textField1 = new JTextField();
    textField1.setEditable(false);
    textField1.setEnabled(false);
    panel5.add(textField1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    final JLabel label2 = new JLabel();
    label2.setText("Explanation");
    panel4.add(label2, new GridConstraints(19, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel6 = new JPanel();
    panel6.setLayout(new GridLayoutManager(1, 1, new Insets(5, 5, 5, 5), -1, -1));
    panel4.add(panel6, new GridConstraints(20, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JScrollPane scrollPane1 = new JScrollPane();
    panel6.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea2 = new JTextArea();
    textArea2.setEditable(false);
    textArea2.setEnabled(true);
    textArea2.setLineWrap(true);
    textArea2.setMargin(new Insets(5, 5, 5, 5));
    textArea2.setRows(5);
    textArea2.setWrapStyleWord(true);
    scrollPane1.setViewportView(textArea2);
    final JLabel label3 = new JLabel();
    label3.setText("");
    panel4.add(label3, new GridConstraints(16, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label4 = new JLabel();
    Font label4Font = this.$$$getFont$$$(null, Font.BOLD, -1, label4.getFont());
    if (label4Font != null) label4.setFont(label4Font);
    label4.setText("Love");
    panel4.add(label4, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb4 = new JRadioButton();
    rb4.setFocusPainted(false);
    rb4.setText("Approval Addiction");
    panel4.add(rb4, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb5 = new JRadioButton();
    rb5.setFocusPainted(false);
    rb5.setText("Love Addiction");
    panel4.add(rb5, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb6 = new JRadioButton();
    rb6.setFocusPainted(false);
    rb6.setText("Fear of Rejection");
    panel4.add(rb6, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label5 = new JLabel();
    Font label5Font = this.$$$getFont$$$(null, Font.BOLD, -1, label5.getFont());
    if (label5Font != null) label5.setFont(label5Font);
    label5.setText("Depression");
    panel4.add(label5, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb13 = new JRadioButton();
    rb13.setFocusPainted(false);
    rb13.setText("Hopelessness");
    panel4.add(rb13, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb14 = new JRadioButton();
    rb14.setFocusPainted(false);
    rb14.setText("Worthlessness/Inferiority");
    panel4.add(rb14, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label6 = new JLabel();
    Font label6Font = this.$$$getFont$$$(null, Font.BOLD, -1, label6.getFont());
    if (label6Font != null) label6.setFont(label6Font);
    label6.setText("Anxiety");
    panel4.add(label6, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb15 = new JRadioButton();
    rb15.setFocusPainted(false);
    rb15.setText("Emotional Perfectionism");
    panel4.add(rb15, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb16 = new JRadioButton();
    rb16.setFocusPainted(false);
    rb16.setText("Anger Phobia");
    panel4.add(rb16, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb17 = new JRadioButton();
    rb17.setFocusPainted(false);
    rb17.setText("Emotophobia");
    panel4.add(rb17, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb18 = new JRadioButton();
    rb18.setFocusPainted(false);
    rb18.setText("Perceived Narcissism");
    panel4.add(rb18, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb19 = new JRadioButton();
    rb19.setFocusPainted(false);
    rb19.setText("Brushfire Fallacy");
    panel4.add(rb19, new GridConstraints(8, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb20 = new JRadioButton();
    rb20.setFocusPainted(false);
    rb20.setText("Spotlight Fallacy");
    panel4.add(rb20, new GridConstraints(9, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb21 = new JRadioButton();
    rb21.setFocusPainted(false);
    rb21.setText("Magical Thinking");
    panel4.add(rb21, new GridConstraints(10, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label7 = new JLabel();
    Font label7Font = this.$$$getFont$$$(null, Font.BOLD, -1, label7.getFont());
    if (label7Font != null) label7.setFont(label7Font);
    label7.setText("Submissiveness");
    panel4.add(label7, new GridConstraints(8, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb7 = new JRadioButton();
    rb7.setFocusPainted(false);
    rb7.setText("Pleasing Others");
    panel4.add(rb7, new GridConstraints(9, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb8 = new JRadioButton();
    rb8.setFocusPainted(false);
    rb8.setText("Conflict Phobia");
    panel4.add(rb8, new GridConstraints(10, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb9 = new JRadioButton();
    rb9.setFocusPainted(false);
    rb9.setText("Self-Blame");
    panel4.add(rb9, new GridConstraints(11, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label8 = new JLabel();
    Font label8Font = this.$$$getFont$$$(null, Font.BOLD, -1, label8.getFont());
    if (label8Font != null) label8.setFont(label8Font);
    label8.setText("Demandingness");
    panel4.add(label8, new GridConstraints(12, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb10 = new JRadioButton();
    rb10.setFocusPainted(false);
    rb10.setText("Other-Blame");
    panel4.add(rb10, new GridConstraints(13, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb11 = new JRadioButton();
    rb11.setFocusPainted(false);
    rb11.setText("Entitlement");
    panel4.add(rb11, new GridConstraints(14, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb12 = new JRadioButton();
    rb12.setFocusPainted(false);
    rb12.setText("Truth");
    panel4.add(rb12, new GridConstraints(15, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label9 = new JLabel();
    Font label9Font = this.$$$getFont$$$(null, Font.BOLD, -1, label9.getFont());
    if (label9Font != null) label9.setFont(label9Font);
    label9.setText("Other");
    panel4.add(label9, new GridConstraints(12, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb22 = new JRadioButton();
    rb22.setFocusPainted(false);
    rb22.setText("Low Frustration Tolerance");
    panel4.add(rb22, new GridConstraints(13, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    rb23 = new JRadioButton();
    rb23.setFocusPainted(false);
    rb23.setText("Superman/Superwoman");
    panel4.add(rb23, new GridConstraints(14, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel7 = new JPanel();
    panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
    panel2.add(panel7, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    panel7.setBorder(BorderFactory.createTitledBorder(""));
    final JScrollPane scrollPane2 = new JScrollPane();
    panel7.add(scrollPane2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    textArea1 = new JTextArea();
    textArea1.setLineWrap(true);
    textArea1.setMargin(new Insets(5, 5, 5, 5));
    textArea1.setWrapStyleWord(true);
    scrollPane2.setViewportView(textArea1);
    final JLabel label10 = new JLabel();
    label10.setText("<html>If this thought were true, why would it be upsetting me?<br>What would it mean to me?</html>");
    panel2.add(label10, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label11 = new JLabel();
    label11.setText("Uncover your self-defeating belief (SDB)");
    panel2.add(label11, new GridConstraints(1, 1, 1, 3, GridConstraints.ANCHOR_SOUTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel8 = new JPanel();
    panel8.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
    panel2.add(panel8, new GridConstraints(0, 0, 1, 4, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    final JLabel label12 = new JLabel();
    Font label12Font = this.$$$getFont$$$(null, Font.BOLD, 20, label12.getFont());
    if (label12Font != null) label12.setFont(label12Font);
    label12.setText("Downward Arrow Technique");
    panel8.add(label12, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final Spacer spacer1 = new Spacer();
    panel8.add(spacer1, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    button1 = new JButton();
    button1.setBorderPainted(false);
    button1.setContentAreaFilled(false);
    button1.setFocusPainted(false);
    button1.setFocusable(false);
    button1.setHorizontalTextPosition(0);
    button1.setIcon(new ImageIcon(getClass().getResource("/help-icon-11-24.png")));
    button1.setOpaque(false);
    button1.setText("");
    button1.setToolTipText("");
    button1.setVerifyInputWhenFocusTarget(false);
    panel8.add(button1, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, new Dimension(32, 32), new Dimension(32, 32), new Dimension(32, 32), 0, false));
    final Spacer spacer2 = new Spacer();
    panel8.add(spacer2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    ButtonGroup buttonGroup;
    buttonGroup = new ButtonGroup();
    buttonGroup.add(rb7);
    buttonGroup.add(rb1);
    buttonGroup.add(rb2);
    buttonGroup.add(rb3);
    buttonGroup.add(rb8);
    buttonGroup.add(rb9);
    buttonGroup.add(rb15);
    buttonGroup.add(rb16);
    buttonGroup.add(rb17);
    buttonGroup.add(rb18);
    buttonGroup.add(rb19);
    buttonGroup.add(rb20);
    buttonGroup.add(rb21);
    buttonGroup.add(rb24);
    buttonGroup.add(rb4);
    buttonGroup.add(rb5);
    buttonGroup.add(rb6);
    buttonGroup.add(rb10);
    buttonGroup.add(rb11);
    buttonGroup.add(rb12);
    buttonGroup.add(rb13);
    buttonGroup.add(rb14);
    buttonGroup.add(rb22);
    buttonGroup.add(rb23);
  }

  /**
   * @noinspection ALL
   */
  private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
    if (currentFont == null) return null;
    String resultName;
    if (fontName == null) {
      resultName = currentFont.getName();
    } else {
      Font testFont = new Font(fontName, Font.PLAIN, 10);
      if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
        resultName = fontName;
      } else {
        resultName = currentFont.getName();
      }
    }
    return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
  }

  public DownwardArrow getData() {
    DownwardArrow da = new DownwardArrow();
    try {
      for (int i = 1; i <= 24; i++) {
        JRadioButton rb = (JRadioButton) FieldUtils.readField(DownwardArrowForm.this, "rb" + i, true);
        if (rb.isSelected()) {
          da.setSdbChoice(i);
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    da.setThoughts(textArea1.getText());

    if (rb24.isSelected()) {
      da.setYourOwn(textField1.getText());
      da.setYourOwnExplanation(textArea2.getText());
    } else {
      da.setYourOwn("");
      da.setYourOwnExplanation("");
    }

    return da;
  }

  public void setData(DownwardArrow downwardArrow) {
    if (downwardArrow.getSdbChoice() > 0) {
      try {
        JRadioButton rb = (JRadioButton) FieldUtils.readField(DownwardArrowForm.this, "rb" + downwardArrow.getSdbChoice(), true);
        rb.setSelected(true);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    textArea1.setText(downwardArrow.getThoughts());
    textField1.setText(downwardArrow.getYourOwn());
    if (downwardArrow.getSdbChoice() != 24 && downwardArrow.getSdbChoice() >= 0) {
      textArea2.setText(Constants.SDB_EXPLANATIONS[downwardArrow.getSdbChoice() - 1]);
    }
  }
}
