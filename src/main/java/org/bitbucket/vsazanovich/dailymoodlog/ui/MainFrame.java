package org.bitbucket.vsazanovich.dailymoodlog.ui;

import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.actions.LoadAction;
import org.bitbucket.vsazanovich.dailymoodlog.actions.OnExitAction;
import org.bitbucket.vsazanovich.dailymoodlog.actions.SaveAction;
import org.bitbucket.vsazanovich.dailymoodlog.actions.UnloadAction;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.nio.file.FileSystem;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 09-09-2019
 */
public class MainFrame extends JFrame {

  public OnExitAction onExitAction;
  public String openFileName;
  public String shortFileName;
  public FileSystem zipFS;
  public int hash;// hash of the project

  public LoadAction loadAction;
  public SaveAction saveAction;
  public UnloadAction unloadAction;

  public JPanel cards;
  public WelcomeForm welcomePanel;
  public EditorPanel editorPanel;
  public PrefsForm prefsPanel;

  public String underPrefsCard;

  private boolean isEnabled;

  public MainFrame(String title) {
    super(title);

    onExitAction = new OnExitAction(this);
    loadAction = new LoadAction(this);
    saveAction = new SaveAction(this);
    unloadAction = new UnloadAction(this);

    setIconImage(new ImageIcon(getClass().getClassLoader().getResource("icon.png")).getImage());

    //Display the window.
    try {
      int width = Constants.PROPS.containsKey(Constants.PROP_WIDTH) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_WIDTH)) : 500;
      int height = Constants.PROPS.containsKey(Constants.PROP_HEIGHT) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_HEIGHT)) : 615;
      setPreferredSize(new Dimension(width, height));
    } catch (Exception ex) {
      setPreferredSize(new Dimension(600, 800));
    }

    try {
      int posX = Constants.PROPS.containsKey(Constants.PROP_POS_X) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_POS_X)) : 0;
      int posY = Constants.PROPS.containsKey(Constants.PROP_POS_Y) ? Integer.parseInt(Constants.PROPS.getProperty(Constants.PROP_POS_Y)) : 0;
      setLocation(posX, posY);
    } catch (Exception ex) {
      setLocation(0, 0);
    }

    KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
      @Override
      public boolean dispatchKeyEvent(KeyEvent e) {

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_O) {
          unloadAction.actionPerformed(new ActionEvent(MainFrame.this, 0, "close"));
          loadAction.actionPerformed(new ActionEvent(MainFrame.this, 0, "open"));
        }

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_S) {
          if (openFileName != null) {
            saveAction.actionPerformed(new ActionEvent(MainFrame.this, 0, "save"));
          }
        }

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_Q) {
          unloadAction.actionPerformed(new ActionEvent(MainFrame.this, 0, "close"));
        }

        if (e.getID() == KeyEvent.KEY_PRESSED && e.isControlDown() && e.getKeyCode() == KeyEvent.VK_P) {
          if (!getVisibleCard().equals(Constants.PROP_PREFS_COMPONENT)) {
            underPrefsCard = getVisibleCard();
            ((CardLayout) cards.getLayout()).show(cards, Constants.PROP_PREFS_COMPONENT);
          }
        }

        //enable CTRL mode to enable removal, dnd is enabled in DNDTabbedPane
        boolean enableRemoval = (e.isControlDown());
        Window[] wws = MainFrame.this.getOwnedWindows();
        for (Window w : wws) {
          if (w.isActive() && w instanceof JDialog) {
            JDialog jd = (JDialog) w;
            Set<DnDTabbedPane> dndPanes = findDndPanes(new HashSet<DnDTabbedPane>(), jd);
            if (enableRemoval != isEnabled) {
              isEnabled = enableRemoval;
              for (DnDTabbedPane pane : dndPanes) {
                if (pane != null) {
                  for (int i = 0; i < pane.getTabCount(); i++) {
                    Component c = pane.getTabComponentAt(i);
                    if (c instanceof ButtonTabComponent) {
                      ButtonTabComponent btc = (ButtonTabComponent) c;
                      btc.enableRemoval(enableRemoval);
                    }
                  }
                }
              }
            }
          }
        }
        return false;
      }
    });

    final AbstractAction escapeAction = new AbstractAction() {
      private static final long serialVersionUID = 1L;

      @Override
      public void actionPerformed(ActionEvent ae) {
        if (getVisibleCard().equals(Constants.PROP_PREFS_COMPONENT)) {
          ((CardLayout) cards.getLayout()).show(cards, underPrefsCard);
        } else {
          setState(Frame.ICONIFIED);
        }
      }
    };

    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "ESCAPE_KEY");
    getRootPane().getActionMap().put("ESCAPE_KEY", escapeAction);

    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    addWindowListener(onExitAction);

    welcomePanel = new WelcomeForm(this);
    editorPanel = new EditorPanel(this);
    prefsPanel = new PrefsForm(this);

    cards = new JPanel(new CardLayout());
    cards.add(welcomePanel.mainPanel, Constants.PROP_WELCOME_COMPONENT);
    cards.add(editorPanel.$$$getRootComponent$$$(), Constants.PROP_EDITOR_COMPONENT);
    cards.add(prefsPanel.mainPanel, Constants.PROP_PREFS_COMPONENT);

    setContentPane(cards);

    ((CardLayout) cards.getLayout()).show(cards, Constants.PROP_WELCOME_COMPONENT);

    load();


    //for testing
//    loadAction.load(new File("C:/tmp/tmp.xml"));
  }

  private Set<DnDTabbedPane> findDndPanes(Set<DnDTabbedPane> set, Container jd) {
    for (Component c : jd.getComponents()) {
      if (c instanceof DnDTabbedPane) {
        set.add((DnDTabbedPane) c);
      } else if (c instanceof Container) {
        set.addAll(findDndPanes(set, (Container) c));
      } else {
        System.out.println(c);
      }
    }
    return set;
  }

  public void load() {
    if (Constants.PROPS.containsKey(Constants.PROP_LAST_OPEN_FILE)) {
      String lastOpenFile = Constants.PROPS.getProperty(Constants.PROP_LAST_OPEN_FILE);
      final File f = new File(lastOpenFile);
      if (f.exists()) {
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            loadAction.load(f);
          }
        });
      }
    }
  }

  public String getVisibleCard() {
    JPanel card = null;
    for (Component comp : cards.getComponents()) {
      if (comp.isVisible() == true) {
        card = (JPanel) comp;
      }
    }
    return card.getName();
  }
}
