package org.bitbucket.vsazanovich.dailymoodlog.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.NegativeThought;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.antiprocrastination.AntiProcrastinationSheetForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.antiprocrastination.LittleStepsForBigFeatsForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.antiprocrastination.PleasurePredictingForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.antiprocrastination.ProblemSolutionListForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.classicalexposure.DistractionForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.classicalexposure.FloodingForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.classicalexposure.GradualExposureForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.classicalexposure.ResponsePreventionForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.cognitiveexposure.CognitiveFloodingForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.cognitiveexposure.FearedFantasyForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.cognitiveexposure.ImageSubstitutionForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.cognitiveexposure.MemoryRescriptingForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.compassion.CompassionBasedTechniqueForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.hiddenemotion.HiddenEmotionTechniqueForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.humor.HumorousImagingForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.humor.ParadoxicalMagnificationForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.humor.ShameAttackingForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.interpersonalexposure.*;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.logic.ProcessVersusOutcomeForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.logic.ThinkingInShadesOfGrayForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.motivational.CostBenefitAnalysisForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.motivational.DevilsAdvocateForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.motivational.ParadoxicalCostBenefitAnalysisForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.quantative.SelfMonitoringForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.quantative.WorryBreaksForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.roleplaying.AcceptanceParadoxForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.roleplaying.ExternalizationOfVoicesForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.semantic.BeSpecificForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.semantic.LetsDefineTermsForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.semantic.SemanticMethodForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.truthbased.ExamineTheEvidenceForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.truthbased.ExperimentalTechniqueForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.truthbased.ReattributionForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.truthbased.SurveyTechniqueForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.uncovering.DownwardArrowForm;
import org.bitbucket.vsazanovich.dailymoodlog.ui.cbt.uncovering.WhatIfTechniqueForm;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;

import static org.bitbucket.vsazanovich.dailymoodlog.Constants.CURRENT_PROJECT;

public class CognitiveBehaviorTherapyDialog extends JDialog {
  private NegativeThought thisInstance;
  private JPanel contentPane;
  private JButton buttonOK;
  private JButton buttonCancel;
  private JList list1;
  private JPanel contentPanel;
  private JPanel panel1;
  private JTextField textField1;
  private KeyEventDispatcher keyEventDispatcher;
  private MainFrame parent;

  public CognitiveBehaviorTherapyDialog(MainFrame owner) {
    super(owner);
    this.parent = owner;
    setContentPane(contentPane);
    setModal(true);
    getRootPane().setDefaultButton(buttonOK);

    setTitle("Cognitive Behavior Debugger");
//    setPreferredSize(new Dimension(800, 900));


    buttonOK.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onOK();
      }
    });

//    buttonCancel.addActionListener(new ActionListener() {
//      public void actionPerformed(ActionEvent e) {
//        onCancel();
//      }
//    });

    textField1.getDocument().addDocumentListener(new DocumentListener() {
      public void changedUpdate(DocumentEvent e) {
        warn();
      }

      public void removeUpdate(DocumentEvent e) {
        warn();
      }

      public void insertUpdate(DocumentEvent e) {
        warn();
      }

      public void warn() {
        buttonOK.setEnabled(!StringUtils.isEmpty(textField1.getText().trim()));
      }
    });

    buttonOK.setEnabled(false);

    list1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
          ((CardLayout) contentPanel.getLayout()).show(contentPanel, list1.getSelectedValue().toString());
        }
      }
    });

    // call onCancel() when cross is clicked
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        onCancel();
      }
    });

    // call onCancel() on ESCAPE
    contentPane.registerKeyboardAction(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        onCancel();
      }
    }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    DownwardArrowForm daf = new DownwardArrowForm(this);
    CostBenefitAnalysisForm cbaf = new CostBenefitAnalysisForm(this);
    PleasurePredictingForm ppf = new PleasurePredictingForm(this);
    WhatIfTechniqueForm wif = new WhatIfTechniqueForm(this);
    CompassionBasedTechniqueForm cb = new CompassionBasedTechniqueForm(this);
    ExamineTheEvidenceForm ex = new ExamineTheEvidenceForm(this);
    ExperimentalTechniqueForm exp = new ExperimentalTechniqueForm(this);
    SurveyTechniqueForm sr = new SurveyTechniqueForm(this);
    ReattributionForm re = new ReattributionForm(this);
    ThinkingInShadesOfGrayForm sh = new ThinkingInShadesOfGrayForm(this);
    ProcessVersusOutcomeForm pr = new ProcessVersusOutcomeForm(this);
    SemanticMethodForm se = new SemanticMethodForm(this);
    LetsDefineTermsForm le = new LetsDefineTermsForm(this);
    BeSpecificForm be = new BeSpecificForm(this);
    SelfMonitoringForm sf = new SelfMonitoringForm(this);
    WorryBreaksForm wo = new WorryBreaksForm(this);
    ShameAttackingForm shame = new ShameAttackingForm(this);
    ParadoxicalMagnificationForm par = new ParadoxicalMagnificationForm(this);
    HumorousImagingForm hum = new HumorousImagingForm(this);
    ExternalizationOfVoicesForm ext = new ExternalizationOfVoicesForm(this);
    AcceptanceParadoxForm acc = new AcceptanceParadoxForm(this);
    ParadoxicalCostBenefitAnalysisForm parcba = new ParadoxicalCostBenefitAnalysisForm(this);
    DevilsAdvocateForm dev = new DevilsAdvocateForm(this);
    LittleStepsForBigFeatsForm lit = new LittleStepsForBigFeatsForm(this);
    AntiProcrastinationSheetForm anti = new AntiProcrastinationSheetForm(this);
    ProblemSolutionListForm pro = new ProblemSolutionListForm(this);
    GradualExposureForm gra = new GradualExposureForm(this);
    FloodingForm flood = new FloodingForm(this);
    ResponsePreventionForm resp = new ResponsePreventionForm(this);
    DistractionForm dist = new DistractionForm(this);
    CognitiveFloodingForm cog = new CognitiveFloodingForm(this);
    ImageSubstitutionForm img = new ImageSubstitutionForm(this);
    MemoryRescriptingForm mem = new MemoryRescriptingForm(this);
    FearedFantasyForm fear = new FearedFantasyForm(this);
    SmileAndHelloPracticeForm smile = new SmileAndHelloPracticeForm(this);
    FlirtingTrainingForm flirt = new FlirtingTrainingForm(this);
    RejectionPracticeForm rej = new RejectionPracticeForm(this);
    SelfDisclosureForm self = new SelfDisclosureForm(this);
    DavidLettermanTechniqueForm david = new DavidLettermanTechniqueForm(this);
    HiddenEmotionTechniqueForm hidden = new HiddenEmotionTechniqueForm(this);

    contentPanel.add(daf.mainPanel, Constants.DOWNWARD_TITLE);
    contentPanel.add(cbaf.mainPanel, Constants.COSTBENEFIT_TITLE);
    contentPanel.add(ppf.mainPanel, Constants.PLEASURE_TITLE);
    contentPanel.add(wif.mainPanel, Constants.WHATIF_TITLE);
    contentPanel.add(cb.mainPanel, Constants.COMPASSION_TITLE);
    contentPanel.add(ex.mainPanel, Constants.EXAMINE_TITLE);
    contentPanel.add(exp.mainPanel, Constants.EXPERIMENTAL_TITLE);
    contentPanel.add(sr.mainPanel, Constants.SURVEY_TITLE);
    contentPanel.add(re.mainPanel, Constants.REATTRIBUTION_TITLE);
    contentPanel.add(sh.mainPanel, Constants.SHADES_TITLE);
    contentPanel.add(pr.mainPanel, Constants.PROCESS_TITLE);
    contentPanel.add(se.mainPanel, Constants.SEMANTIC_TITLE);
    contentPanel.add(le.mainPanel, Constants.LETS_TITLE);
    contentPanel.add(be.mainPanel, Constants.BE_TITLE);
    contentPanel.add(sf.mainPanel, Constants.SELF_TITLE);
    contentPanel.add(wo.mainPanel, Constants.WORRY_TITLE);
    contentPanel.add(shame.mainPanel, Constants.SHAME_TITLE);
    contentPanel.add(par.mainPanel, Constants.PAR_TITLE);
    contentPanel.add(hum.mainPanel, Constants.HUM_TITLE);
    contentPanel.add(ext.mainPanel, Constants.EXT_TITLE);
    contentPanel.add(acc.mainPanel, Constants.ACC_TITLE);
    contentPanel.add(parcba.mainPanel, Constants.PARCBA_TITLE);
    contentPanel.add(dev.mainPanel, Constants.DEV_TITLE);
    contentPanel.add(lit.mainPanel, Constants.LIT_TITLE);
    contentPanel.add(anti.mainPanel, Constants.ANTI_TITLE);
    contentPanel.add(pro.mainPanel, Constants.PRO_TITLE);
    contentPanel.add(gra.mainPanel, Constants.GRA_TITLE);
    contentPanel.add(flood.mainPanel, Constants.FLOOD_TITLE);
    contentPanel.add(resp.mainPanel, Constants.RESP_TITLE);
    contentPanel.add(dist.mainPanel, Constants.DIST_TITLE);
    contentPanel.add(cog.mainPanel, Constants.COG_TITLE);
    contentPanel.add(img.mainPanel, Constants.IMG_TITLE);
    contentPanel.add(mem.mainPanel, Constants.MEM_TITLE);
    contentPanel.add(fear.mainPanel, Constants.FEA_TITLE);
    contentPanel.add(smile.mainPanel, Constants.SMILE_TITLE);
    contentPanel.add(flirt.mainPanel, Constants.FLIRT_TITLE);
    contentPanel.add(rej.mainPanel, Constants.REJ_TITLE);
    contentPanel.add(self.mainPanel, Constants.SELFD_TITLE);
    contentPanel.add(david.mainPanel, Constants.DAVID_TITLE);
    contentPanel.add(hidden.mainPanel, Constants.HIDDEN_TITLE);

    ((DefaultListModel) list1.getModel()).addElement(daf);
    ((DefaultListModel) list1.getModel()).addElement(cbaf);
    ((DefaultListModel) list1.getModel()).addElement(ppf);
    ((DefaultListModel) list1.getModel()).addElement(wif);
    ((DefaultListModel) list1.getModel()).addElement(cb);
    ((DefaultListModel) list1.getModel()).addElement(ex);
    ((DefaultListModel) list1.getModel()).addElement(exp);
    ((DefaultListModel) list1.getModel()).addElement(sr);
    ((DefaultListModel) list1.getModel()).addElement(re);
    ((DefaultListModel) list1.getModel()).addElement(sh);
    ((DefaultListModel) list1.getModel()).addElement(pr);
    ((DefaultListModel) list1.getModel()).addElement(se);
    ((DefaultListModel) list1.getModel()).addElement(le);
    ((DefaultListModel) list1.getModel()).addElement(be);
    ((DefaultListModel) list1.getModel()).addElement(sf);
    ((DefaultListModel) list1.getModel()).addElement(wo);
    ((DefaultListModel) list1.getModel()).addElement(shame);
    ((DefaultListModel) list1.getModel()).addElement(par);
    ((DefaultListModel) list1.getModel()).addElement(hum);
    ((DefaultListModel) list1.getModel()).addElement(ext);
    ((DefaultListModel) list1.getModel()).addElement(acc);
    ((DefaultListModel) list1.getModel()).addElement(parcba);
    ((DefaultListModel) list1.getModel()).addElement(dev);
    ((DefaultListModel) list1.getModel()).addElement(lit);
    ((DefaultListModel) list1.getModel()).addElement(anti);
    ((DefaultListModel) list1.getModel()).addElement(pro);
    ((DefaultListModel) list1.getModel()).addElement(gra);
    ((DefaultListModel) list1.getModel()).addElement(flood);
    ((DefaultListModel) list1.getModel()).addElement(resp);
    ((DefaultListModel) list1.getModel()).addElement(dist);
    ((DefaultListModel) list1.getModel()).addElement(cog);
    ((DefaultListModel) list1.getModel()).addElement(img);
    ((DefaultListModel) list1.getModel()).addElement(mem);
    ((DefaultListModel) list1.getModel()).addElement(fear);
    ((DefaultListModel) list1.getModel()).addElement(smile);
    ((DefaultListModel) list1.getModel()).addElement(flirt);
    ((DefaultListModel) list1.getModel()).addElement(rej);
    ((DefaultListModel) list1.getModel()).addElement(self);
    ((DefaultListModel) list1.getModel()).addElement(david);
    ((DefaultListModel) list1.getModel()).addElement(hidden);

    list1.setSelectedIndex(0);

    //for testing
//    ((CardLayout) contentPanel.getLayout()).show(contentPanel, Constants.COMPASSION_TITLE);
  }

  private void onOK() {
    NegativeThought nt = getData();

    CURRENT_PROJECT.getThoughts().put(nt.getTime(), nt);
    dispose();
    parent.saveAction.saveText();

    if (thisInstance == null) {
      ((DefaultListModel) Constants.FRAME.editorPanel.thoughtsList.getModel()).insertElementAt(nt, 0);
      Constants.FRAME.editorPanel.thoughtsList.setSelectedIndex(0);
    }
  }

  private NegativeThought getData() {
    NegativeThought nt = thisInstance;
    if (nt == null) {
      nt = new NegativeThought();
      nt.setTime(System.currentTimeMillis());
    }
    nt.setTitle(textField1.getText().trim());
    for (int i = 0; i < list1.getModel().getSize(); i++) {
      Object o = list1.getModel().getElementAt(i);
      //1
      if (o instanceof DownwardArrowForm) {
        DownwardArrowForm daf = (DownwardArrowForm) o;
        nt.setDownwardArrow(daf.getData());
      }
      //2
      if (o instanceof CostBenefitAnalysisForm) {
        CostBenefitAnalysisForm daf = (CostBenefitAnalysisForm) o;
        nt.setCostBenefitAnalysis(daf.getData());
      }
      //3
      if (o instanceof PleasurePredictingForm) {
        PleasurePredictingForm daf = (PleasurePredictingForm) o;
        nt.setPleasurePredictingSheet(daf.getData());
      }
      //4
      if (o instanceof WhatIfTechniqueForm) {
        WhatIfTechniqueForm daf = (WhatIfTechniqueForm) o;
        nt.setWhatIfTechniqueForm(daf.getData());
      }
      //5
      if (o instanceof CompassionBasedTechniqueForm) {
        CompassionBasedTechniqueForm daf = (CompassionBasedTechniqueForm) o;
        nt.setCompassionBasedTechniqueForm(daf.getData());
      }
      //6
      if (o instanceof ExamineTheEvidenceForm) {
        ExamineTheEvidenceForm daf = (ExamineTheEvidenceForm) o;
        nt.setExamineTheEvidenceForm(daf.getData());
      }
      //7
      if (o instanceof ExperimentalTechniqueForm) {
        ExperimentalTechniqueForm daf = (ExperimentalTechniqueForm) o;
        nt.setExperimentalTechniqueForm(daf.getData());
      }
      //8
      if (o instanceof SurveyTechniqueForm) {
        SurveyTechniqueForm daf = (SurveyTechniqueForm) o;
        nt.setSurveyTechniqueForm(daf.getData());
      }
      //9
      if (o instanceof ReattributionForm) {
        ReattributionForm daf = (ReattributionForm) o;
        nt.setReattributionForm(daf.getData());
      }
      //10
      if (o instanceof ThinkingInShadesOfGrayForm) {
        ThinkingInShadesOfGrayForm daf = (ThinkingInShadesOfGrayForm) o;
        nt.setThinkingInShadesOfGrayForm(daf.getData());
      }
      //11
      if (o instanceof ProcessVersusOutcomeForm) {
        ProcessVersusOutcomeForm daf = (ProcessVersusOutcomeForm) o;
        nt.setProcessVersusOutcomeForm(daf.getData());
      }
      //12
      if (o instanceof SemanticMethodForm) {
        SemanticMethodForm daf = (SemanticMethodForm) o;
        nt.setSemanticMethodForm(daf.getData());
      }
      //13
      if (o instanceof LetsDefineTermsForm) {
        LetsDefineTermsForm daf = (LetsDefineTermsForm) o;
        nt.setLetsDefineTermsForm(daf.getData());
      }
      //14
      if (o instanceof BeSpecificForm) {
        BeSpecificForm daf = (BeSpecificForm) o;
        nt.setBeSpecificForm(daf.getData());
      }
      //15
      if (o instanceof SelfMonitoringForm) {
        SelfMonitoringForm daf = (SelfMonitoringForm) o;
        nt.setSelfMonitoringForm(daf.getData());
      }
      //16
      if (o instanceof WorryBreaksForm) {
        WorryBreaksForm daf = (WorryBreaksForm) o;
        nt.setWorryBreaksForm(daf.getData());
      }
      //17
      if (o instanceof ShameAttackingForm) {
        ShameAttackingForm daf = (ShameAttackingForm) o;
        nt.setShameAttackingForm(daf.getData());
      }
      //18
      if (o instanceof ParadoxicalMagnificationForm) {
        ParadoxicalMagnificationForm daf = (ParadoxicalMagnificationForm) o;
        nt.setParadoxicalMagnificationForm(daf.getData());
      }
      //19
      if (o instanceof HumorousImagingForm) {
        HumorousImagingForm daf = (HumorousImagingForm) o;
        nt.setHumorousImagingForm(daf.getData());
      }
      //20
      if (o instanceof ExternalizationOfVoicesForm) {
        ExternalizationOfVoicesForm daf = (ExternalizationOfVoicesForm) o;
        nt.setExternalizationOfVoicesForm(daf.getData());
      }
      //21
      if (o instanceof AcceptanceParadoxForm) {
        AcceptanceParadoxForm daf = (AcceptanceParadoxForm) o;
        nt.setAcceptanceParadoxForm(daf.getData());
      }
      //22
      if (o instanceof ParadoxicalCostBenefitAnalysisForm) {
        ParadoxicalCostBenefitAnalysisForm daf = (ParadoxicalCostBenefitAnalysisForm) o;
        nt.setParadoxicalCostBenefitAnalysisForm(daf.getData());
      }
      //23
      if (o instanceof DevilsAdvocateForm) {
        DevilsAdvocateForm daf = (DevilsAdvocateForm) o;
        nt.setDevilsAdvocateForm(daf.getData());
      }
      //24
      if (o instanceof LittleStepsForBigFeatsForm) {
        LittleStepsForBigFeatsForm daf = (LittleStepsForBigFeatsForm) o;
        nt.setLittleStepsForBigFeatsForm(daf.getData());
      }
      //25
      if (o instanceof AntiProcrastinationSheetForm) {
        AntiProcrastinationSheetForm daf = (AntiProcrastinationSheetForm) o;
        nt.setAntiProcrastinationSheetForm(daf.getData());
      }
      //26
      if (o instanceof ProblemSolutionListForm) {
        ProblemSolutionListForm daf = (ProblemSolutionListForm) o;
        nt.setProblemSolutionListForm(daf.getData());
      }
      //27
      if (o instanceof GradualExposureForm) {
        GradualExposureForm daf = (GradualExposureForm) o;
        nt.setGradualExposureForm(daf.getData());
      }
      //28
      if (o instanceof FloodingForm) {
        FloodingForm daf = (FloodingForm) o;
        nt.setFloodingForm(daf.getData());
      }
      //29
      if (o instanceof ResponsePreventionForm) {
        ResponsePreventionForm daf = (ResponsePreventionForm) o;
        nt.setResponsePreventionForm(daf.getData());
      }
      //30
      if (o instanceof DistractionForm) {
        DistractionForm daf = (DistractionForm) o;
        nt.setDistractionForm(daf.getData());
      }
      //31
      if (o instanceof CognitiveFloodingForm) {
        CognitiveFloodingForm daf = (CognitiveFloodingForm) o;
        nt.setCognitiveFloodingForm(daf.getData());
      }
      //32
      if (o instanceof ImageSubstitutionForm) {
        ImageSubstitutionForm daf = (ImageSubstitutionForm) o;
        nt.setImageSubstitutionForm(daf.getData());
      }
      //33
      if (o instanceof MemoryRescriptingForm) {
        MemoryRescriptingForm daf = (MemoryRescriptingForm) o;
        nt.setMemoryRescriptingForm(daf.getData());
      }
      //34
      if (o instanceof FearedFantasyForm) {
        FearedFantasyForm daf = (FearedFantasyForm) o;
        nt.setFearedFantasyForm(daf.getData());
      }
      //35
      if (o instanceof SmileAndHelloPracticeForm) {
        SmileAndHelloPracticeForm daf = (SmileAndHelloPracticeForm) o;
        nt.setSmileAndHelloPracticeForm(daf.getData());
      }
      //36
      if (o instanceof FlirtingTrainingForm) {
        FlirtingTrainingForm daf = (FlirtingTrainingForm) o;
        nt.setFlirtingTrainingForm(daf.getData());
      }
      //37
      if (o instanceof RejectionPracticeForm) {
        RejectionPracticeForm daf = (RejectionPracticeForm) o;
        nt.setRejectionPracticeForm(daf.getData());
      }
      //38
      if (o instanceof SelfDisclosureForm) {
        SelfDisclosureForm daf = (SelfDisclosureForm) o;
        nt.setSelfDisclosureForm(daf.getData());
      }
      //39
      if (o instanceof DavidLettermanTechniqueForm) {
        DavidLettermanTechniqueForm daf = (DavidLettermanTechniqueForm) o;
        nt.setDavidLettermanTechniqueForm(daf.getData());
      }
      //40
      if (o instanceof HiddenEmotionTechniqueForm) {
        HiddenEmotionTechniqueForm daf = (HiddenEmotionTechniqueForm) o;
        nt.setHiddenEmotionTechniqueForm(daf.getData());
      }
    }
    return nt;
  }

  public void setData(NegativeThought nt) {
    this.thisInstance = nt;
    textField1.setText(nt.getTitle());
    for (int i = 0; i < list1.getModel().getSize(); i++) {
      Object o = list1.getModel().getElementAt(i);
      //1
      if (o instanceof DownwardArrowForm) {
        DownwardArrowForm daf = (DownwardArrowForm) o;
        if (nt.getDownwardArrow() != null) {
          daf.setData(nt.getDownwardArrow());
        }
      }
      //2
      if (o instanceof CostBenefitAnalysisForm) {
        CostBenefitAnalysisForm daf = (CostBenefitAnalysisForm) o;
        if (nt.getCostBenefitAnalysis() != null) {
          daf.setData(nt.getCostBenefitAnalysis());
        }
      }
      //3
      if (o instanceof PleasurePredictingForm) {
        PleasurePredictingForm daf = (PleasurePredictingForm) o;
        if (nt.getPleasurePredictingSheet() != null) {
          daf.setData(nt.getPleasurePredictingSheet());
        }
      }
      //4
      if (o instanceof WhatIfTechniqueForm) {
        WhatIfTechniqueForm daf = (WhatIfTechniqueForm) o;
        if (nt.getWhatIfTechniqueForm() != null) {
          daf.setData(nt.getWhatIfTechniqueForm());
        }
      }
      //5
      if (o instanceof CompassionBasedTechniqueForm) {
        CompassionBasedTechniqueForm daf = (CompassionBasedTechniqueForm) o;
        if (nt.getCompassionBasedTechniqueForm() != null) {
          daf.setData(nt.getCompassionBasedTechniqueForm());
        }
      }
      //6
      if (o instanceof ExamineTheEvidenceForm) {
        ExamineTheEvidenceForm daf = (ExamineTheEvidenceForm) o;
        if (nt.getExamineTheEvidenceForm() != null) {
          daf.setData(nt.getExamineTheEvidenceForm());
        }
      }
      //7
      if (o instanceof ExperimentalTechniqueForm) {
        ExperimentalTechniqueForm daf = (ExperimentalTechniqueForm) o;
        if (nt.getExperimentalTechniqueForm() != null) {
          daf.setData(nt.getExperimentalTechniqueForm());
        }
      }
      //8
      if (o instanceof SurveyTechniqueForm) {
        SurveyTechniqueForm daf = (SurveyTechniqueForm) o;
        if (nt.getSurveyTechniqueForm() != null) {
          daf.setData(nt.getSurveyTechniqueForm());
        }
      }
      //9
      if (o instanceof ReattributionForm) {
        ReattributionForm daf = (ReattributionForm) o;
        if (nt.getReattributionForm() != null) {
          daf.setData(nt.getReattributionForm());
        }
      }
      //10
      if (o instanceof ThinkingInShadesOfGrayForm) {
        ThinkingInShadesOfGrayForm daf = (ThinkingInShadesOfGrayForm) o;
        if (nt.getThinkingInShadesOfGrayForm() != null) {
          daf.setData(nt.getThinkingInShadesOfGrayForm());
        }
      }
      //11
      if (o instanceof ProcessVersusOutcomeForm) {
        ProcessVersusOutcomeForm daf = (ProcessVersusOutcomeForm) o;
        if (nt.getProcessVersusOutcomeForm() != null) {
          daf.setData(nt.getProcessVersusOutcomeForm());
        }
      }
      //12
      if (o instanceof SemanticMethodForm) {
        SemanticMethodForm daf = (SemanticMethodForm) o;
        if (nt.getSemanticMethodForm() != null) {
          daf.setData(nt.getSemanticMethodForm());
        }
      }
      //13
      if (o instanceof LetsDefineTermsForm) {
        LetsDefineTermsForm daf = (LetsDefineTermsForm) o;
        if (nt.getLetsDefineTermsForm() != null) {
          daf.setData(nt.getLetsDefineTermsForm());
        }
      }
      //14
      if (o instanceof BeSpecificForm) {
        BeSpecificForm daf = (BeSpecificForm) o;
        if (nt.getBeSpecificForm() != null) {
          daf.setData(nt.getBeSpecificForm());
        }
      }
      //15
      if (o instanceof SelfMonitoringForm) {
        SelfMonitoringForm daf = (SelfMonitoringForm) o;
        if (nt.getSelfMonitoringForm() != null) {
          daf.setData(nt.getSelfMonitoringForm());
        }
      }
      //16
      if (o instanceof WorryBreaksForm) {
        WorryBreaksForm daf = (WorryBreaksForm) o;
        if (nt.getWorryBreaksForm() != null) {
          daf.setData(nt.getWorryBreaksForm());
        }
      }
      //17
      if (o instanceof ShameAttackingForm) {
        ShameAttackingForm daf = (ShameAttackingForm) o;
        if (nt.getShameAttackingForm() != null) {
          daf.setData(nt.getShameAttackingForm());
        }
      }
      //18
      if (o instanceof ParadoxicalMagnificationForm) {
        ParadoxicalMagnificationForm daf = (ParadoxicalMagnificationForm) o;
        if (nt.getParadoxicalMagnificationForm() != null) {
          daf.setData(nt.getParadoxicalMagnificationForm());
        }
      }
      //19
      if (o instanceof HumorousImagingForm) {
        HumorousImagingForm daf = (HumorousImagingForm) o;
        if (nt.getHumorousImagingForm() != null) {
          daf.setData(nt.getHumorousImagingForm());
        }
      }
      //20
      if (o instanceof ExternalizationOfVoicesForm) {
        ExternalizationOfVoicesForm daf = (ExternalizationOfVoicesForm) o;
        if (nt.getExternalizationOfVoicesForm() != null) {
          daf.setData(nt.getExternalizationOfVoicesForm());
        }
      }
      //21
      if (o instanceof AcceptanceParadoxForm) {
        AcceptanceParadoxForm daf = (AcceptanceParadoxForm) o;
        if (nt.getAcceptanceParadoxForm() != null) {
          daf.setData(nt.getAcceptanceParadoxForm());
        }
      }
      //22
      if (o instanceof ParadoxicalCostBenefitAnalysisForm) {
        ParadoxicalCostBenefitAnalysisForm daf = (ParadoxicalCostBenefitAnalysisForm) o;
        if (nt.getParadoxicalCostBenefitAnalysisForm() != null) {
          daf.setData(nt.getParadoxicalCostBenefitAnalysisForm());
        }
      }
      //23
      if (o instanceof DevilsAdvocateForm) {
        DevilsAdvocateForm daf = (DevilsAdvocateForm) o;
        if (nt.getDevilsAdvocateForm() != null) {
          daf.setData(nt.getDevilsAdvocateForm());
        }
      }
      //24
      if (o instanceof LittleStepsForBigFeatsForm) {
        LittleStepsForBigFeatsForm daf = (LittleStepsForBigFeatsForm) o;
        if (nt.getLittleStepsForBigFeatsForm() != null) {
          daf.setData(nt.getLittleStepsForBigFeatsForm());
        }
      }
      //25
      if (o instanceof AntiProcrastinationSheetForm) {
        AntiProcrastinationSheetForm daf = (AntiProcrastinationSheetForm) o;
        if (nt.getAntiProcrastinationSheetForm() != null) {
          daf.setData(nt.getAntiProcrastinationSheetForm());
        }
      }
      //26
      if (o instanceof ProblemSolutionListForm) {
        ProblemSolutionListForm daf = (ProblemSolutionListForm) o;
        if (nt.getProblemSolutionListForm() != null) {
          daf.setData(nt.getProblemSolutionListForm());
        }
      }
      //27
      if (o instanceof GradualExposureForm) {
        GradualExposureForm daf = (GradualExposureForm) o;
        if (nt.getGradualExposureForm() != null) {
          daf.setData(nt.getGradualExposureForm());
        }
      }
      //28
      if (o instanceof FloodingForm) {
        FloodingForm daf = (FloodingForm) o;
        if (nt.getFloodingForm() != null) {
          daf.setData(nt.getFloodingForm());
        }
      }
      //29
      if (o instanceof ResponsePreventionForm) {
        ResponsePreventionForm daf = (ResponsePreventionForm) o;
        if (nt.getResponsePreventionForm() != null) {
          daf.setData(nt.getResponsePreventionForm());
        }
      }
      //30
      if (o instanceof DistractionForm) {
        DistractionForm daf = (DistractionForm) o;
        if (nt.getDistractionForm() != null) {
          daf.setData(nt.getDistractionForm());
        }
      }
      //31
      if (o instanceof CognitiveFloodingForm) {
        CognitiveFloodingForm daf = (CognitiveFloodingForm) o;
        if (nt.getCognitiveFloodingForm() != null) {
          daf.setData(nt.getCognitiveFloodingForm());
        }
      }
      //32
      if (o instanceof ImageSubstitutionForm) {
        ImageSubstitutionForm daf = (ImageSubstitutionForm) o;
        if (nt.getImageSubstitutionForm() != null) {
          daf.setData(nt.getImageSubstitutionForm());
        }
      }
      //33
      if (o instanceof MemoryRescriptingForm) {
        MemoryRescriptingForm daf = (MemoryRescriptingForm) o;
        if (nt.getMemoryRescriptingForm() != null) {
          daf.setData(nt.getMemoryRescriptingForm());
        }
      }
      //34
      if (o instanceof FearedFantasyForm) {
        FearedFantasyForm daf = (FearedFantasyForm) o;
        if (nt.getFearedFantasyForm() != null) {
          daf.setData(nt.getFearedFantasyForm());
        }
      }
      //35
      if (o instanceof SmileAndHelloPracticeForm) {
        SmileAndHelloPracticeForm daf = (SmileAndHelloPracticeForm) o;
        if (nt.getSmileAndHelloPracticeForm() != null) {
          daf.setData(nt.getSmileAndHelloPracticeForm());
        }
      }
      //36
      if (o instanceof FlirtingTrainingForm) {
        FlirtingTrainingForm daf = (FlirtingTrainingForm) o;
        if (nt.getFlirtingTrainingForm() != null) {
          daf.setData(nt.getFlirtingTrainingForm());
        }
      }
      //37
      if (o instanceof RejectionPracticeForm) {
        RejectionPracticeForm daf = (RejectionPracticeForm) o;
        if (nt.getRejectionPracticeForm() != null) {
          daf.setData(nt.getRejectionPracticeForm());
        }
      }
      //38
      if (o instanceof SelfDisclosureForm) {
        SelfDisclosureForm daf = (SelfDisclosureForm) o;
        if (nt.getSelfDisclosureForm() != null) {
          daf.setData(nt.getSelfDisclosureForm());
        }
      }
      //39
      if (o instanceof DavidLettermanTechniqueForm) {
        DavidLettermanTechniqueForm daf = (DavidLettermanTechniqueForm) o;
        if (nt.getDavidLettermanTechniqueForm() != null) {
          daf.setData(nt.getDavidLettermanTechniqueForm());
        }
      }
      //40
      if (o instanceof HiddenEmotionTechniqueForm) {
        HiddenEmotionTechniqueForm daf = (HiddenEmotionTechniqueForm) o;
        if (nt.getHiddenEmotionTechniqueForm() != null) {
          daf.setData(nt.getHiddenEmotionTechniqueForm());
        }
      }
    }
  }

  private void onCancel() {
    // add your code here if necessary
    dispose();
  }

  public static void main(String[] args) {
    CognitiveBehaviorTherapyDialog dialog = new CognitiveBehaviorTherapyDialog(null);
    dialog.pack();
    dialog.setVisible(true);
    System.exit(0);
  }

  {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
    $$$setupUI$$$();
  }

  /**
   * Method generated by IntelliJ IDEA GUI Designer
   * >>> IMPORTANT!! <<<
   * DO NOT edit this method OR call it in your code!
   *
   * @noinspection ALL
   */
  private void $$$setupUI$$$() {
    contentPane = new JPanel();
    contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
    final JPanel panel2 = new JPanel();
    panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
    contentPane.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
    final Spacer spacer1 = new Spacer();
    panel2.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    final JPanel panel3 = new JPanel();
    panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
    panel2.add(panel3, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    buttonOK = new JButton();
    buttonOK.setText("Save");
    panel3.add(buttonOK, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel4 = new JPanel();
    panel4.setLayout(new BorderLayout(0, 0));
    contentPane.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    panel1 = new JPanel();
    panel1.setLayout(new BorderLayout(0, 0));
    panel4.add(panel1, BorderLayout.CENTER);
    final JPanel panel5 = new JPanel();
    panel5.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 10, 0), -1, -1));
    panel1.add(panel5, BorderLayout.NORTH);
    final JLabel label1 = new JLabel();
    label1.setText("Negative thought");
    panel5.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JLabel label2 = new JLabel();
    label2.setForeground(new Color(-4521963));
    label2.setHorizontalAlignment(10);
    label2.setIconTextGap(4);
    label2.setText("*");
    panel5.add(label2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, new Dimension(5, -1), 0, false));
    textField1 = new JTextField();
    panel5.add(textField1, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    final JPanel panel6 = new JPanel();
    panel6.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
    panel1.add(panel6, BorderLayout.WEST);
    final JLabel label3 = new JLabel();
    Font label3Font = this.$$$getFont$$$(null, Font.BOLD, 20, label3.getFont());
    if (label3Font != null) label3.setFont(label3Font);
    label3.setText("Techniques");
    panel6.add(label3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    final JPanel panel7 = new JPanel();
    panel7.setLayout(new BorderLayout(0, 0));
    panel6.add(panel7, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    panel7.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
    list1 = new JList();
    list1.setFocusable(false);
    Font list1Font = this.$$$getFont$$$(null, -1, 12, list1.getFont());
    if (list1Font != null) list1.setFont(list1Font);
    final DefaultListModel defaultListModel1 = new DefaultListModel();
    list1.setModel(defaultListModel1);
    list1.setSelectionMode(0);
    panel7.add(list1, BorderLayout.CENTER);
    contentPanel = new JPanel();
    contentPanel.setLayout(new CardLayout(0, 0));
    contentPanel.setMinimumSize(new Dimension(600, 600));
    panel1.add(contentPanel, BorderLayout.CENTER);
    contentPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0), null));
  }

  /**
   * @noinspection ALL
   */
  private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
    if (currentFont == null) return null;
    String resultName;
    if (fontName == null) {
      resultName = currentFont.getName();
    } else {
      Font testFont = new Font(fontName, Font.PLAIN, 10);
      if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
        resultName = fontName;
      } else {
        resultName = currentFont.getName();
      }
    }
    return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
  }

  /**
   * @noinspection ALL
   */
  public JComponent $$$getRootComponent$$$() {
    return contentPane;
  }

}
