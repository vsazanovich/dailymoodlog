package org.bitbucket.vsazanovich.dailymoodlog.actions;

import org.apache.commons.io.FilenameUtils;
import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.DMLProject;
import org.bitbucket.vsazanovich.dailymoodlog.ui.MainFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.beans.XMLEncoder;
import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 09-09-2019
 */
public class SaveAction extends WindowAdapter implements ActionListener {
  protected MainFrame frame;
  private ExecutorService executorService = Executors.newCachedThreadPool();
  SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  public SaveAction(MainFrame f) {
    this.frame = f;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    saveText();
  }

  public void saveText() {
    String openFN = frame.openFileName;
    String project = serializeStory();
    int newHash = Constants.getHash(project);
    if (newHash != frame.hash) {
      final long time = System.currentTimeMillis();
      try {
        File f = new File(FilenameUtils.getFullPath(frame.openFileName));
        f.mkdir();
        Writer writer = new OutputStreamWriter(new FileOutputStream(frame.openFileName), "UTF-8");
        BufferedWriter fout = new BufferedWriter(writer);
        fout.write(project);
        fout.close();
        frame.hash = newHash;
      } catch (IOException ex) {
        ex.printStackTrace();
      }
      frame.setTitle(frame.shortFileName + " - " + format.format(new Date(time)));
      executorService.execute(new Runnable() {
        @Override
        public void run() {
          saveAsync(time, openFN, project);
        }
      });

    }
  }

  protected String serializeStory() {
    return serialize(Constants.CURRENT_PROJECT);
  }

  protected String serialize(DMLProject sp) {
    String ret = null;
    try {
      ByteArrayOutputStream fos = new ByteArrayOutputStream();
      XMLEncoder xmlEncoder = new XMLEncoder(fos);
      xmlEncoder.writeObject(sp);
      xmlEncoder.close();
      ret = new String(fos.toByteArray(), "UTF-8");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return ret;
  }

  private void saveAsync(long time, String openFN, String project) {
    try {
      Path path = Paths.get(openFN + ".zip");
      Map<String, String> zipProps = new HashMap<>();
      zipProps.put("create", "true");
      URI uri = URI.create("jar:" + path.toUri());

      if (frame.zipFS == null || !frame.zipFS.isOpen()) {
        frame.zipFS = FileSystems.newFileSystem(uri, zipProps);
      }
      cleanup();
      backup(time, project);
      frame.zipFS.close();

      System.out.println("Backup took " + (System.currentTimeMillis() - time));

    } catch (Exception ex) {
      frame.setTitle(ex.getMessage());
      ex.printStackTrace();
    }
  }


  private void cleanup() {
    try {
      SortedSet<Path> names = new TreeSet<>();
      Stream<Path> stream = Files.list(frame.zipFS.getPath("/"));
      stream.forEach((path -> {
        names.add(path.getFileName());
      }));
      if (names.size() >= Constants.BACKUP_LIMIT) {
        while (names.size() >= Constants.BACKUP_LIMIT) {
          Path path2delete = names.first();
          Files.delete(path2delete);
          names.remove(path2delete);
          System.out.println("removed " + path2delete);
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private void backup(long timestamp, String project) {
    try {
      Path nf = frame.zipFS.getPath(timestamp + ".xml");
      try (Writer writer = Files.newBufferedWriter(nf, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) {
        writer.write(project);
      }
      System.out.println("added " + nf);
//      fs.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}


