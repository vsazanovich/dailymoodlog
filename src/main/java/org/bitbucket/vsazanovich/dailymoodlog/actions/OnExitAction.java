package org.bitbucket.vsazanovich.dailymoodlog.actions;

import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.ui.MainFrame;

import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 09-09-2019
 */
public class OnExitAction extends SaveAction {

  public OnExitAction(MainFrame f) {
    super(f);
  }

  @Override
  public void windowClosing(WindowEvent e) {

    Constants.PROPS.setProperty(Constants.PROP_WIDTH, String.valueOf(frame.getWidth()));
    Constants.PROPS.setProperty(Constants.PROP_HEIGHT, String.valueOf(frame.getHeight()));
    Constants.PROPS.setProperty(Constants.PROP_POS_X, String.valueOf((int) frame.getLocation().getX()));
    Constants.PROPS.setProperty(Constants.PROP_POS_Y, String.valueOf((int) frame.getLocation().getY()));
    Constants.PROPS.setProperty(Constants.PROP_DIVIDER_LOCATION, String.valueOf(frame.editorPanel.surveysSplitPane.getDividerLocation()));
    Constants.PROPS.setProperty(Constants.PROP_SELECTED_TAB, String.valueOf(frame.editorPanel.tabbedPane1.getSelectedIndex()));
//    Constants.PROPS.setProperty(Constants.PROP_LISTS_DIVIDER_LOCATION, String.valueOf(frame.editorPanel.listsSplitPane.getDividerLocation()));

    if (frame.openFileName != null) {
      Constants.PROPS.setProperty(Constants.PROP_LAST_OPEN_FILE, frame.openFileName);
      saveText();
    } else {
      Constants.PROPS.remove(Constants.PROP_LAST_OPEN_FILE);
    }

    try {
      FileOutputStream fos = new FileOutputStream(System.getProperty("user.home") + File.separator + Constants.PROPS_FILE_NAME);
      Constants.PROPS.store(fos, "");
    } catch (Exception e1) {
      e1.printStackTrace();
    }

    e.getWindow().dispose();
    System.exit(0);
  }
}
