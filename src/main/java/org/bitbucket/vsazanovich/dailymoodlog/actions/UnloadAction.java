package org.bitbucket.vsazanovich.dailymoodlog.actions;

import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.ui.MainFrame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 10-09-2019
 */
public class UnloadAction extends SaveAction implements ActionListener {
  public UnloadAction(MainFrame f) {
    super(f);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (frame.openFileName != null) {
      saveText();
      frame.openFileName = null;
      frame.editorPanel.unload();
      frame.setTitle(Constants.TITLE);
      ((CardLayout) frame.cards.getLayout()).show(frame.cards, Constants.PROP_WELCOME_COMPONENT);
    }
  }
}
