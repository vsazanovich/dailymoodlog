package org.bitbucket.vsazanovich.dailymoodlog.actions;

import org.bitbucket.vsazanovich.dailymoodlog.Constants;
import org.bitbucket.vsazanovich.dailymoodlog.model.DMLProject;
import org.bitbucket.vsazanovich.dailymoodlog.ui.MainFrame;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * Author: Vitaly Sazanovich
 * Email: vitaly.sazanovich@gmail.com
 * Date: 10-09-2019
 */
public class LoadAction extends SaveAction implements ActionListener {

  public LoadAction(MainFrame f) {
    super(f);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    //load
    JFileChooser fc;
    if (Constants.PROPS.containsKey(Constants.PROP_LAST_OPEN_DIR)) {
      try {
        fc = new JFileChooser(Constants.PROPS.getProperty(Constants.PROP_LAST_OPEN_DIR));
      } catch (Exception ex) {
        fc = new JFileChooser();
      }
    } else {
      fc = new JFileChooser();
    }

    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    FileNameExtensionFilter filter = new FileNameExtensionFilter("XML (*.xml)", "xml", "xml");
    fc.setFileFilter(filter);

    int returnVal = fc.showOpenDialog(frame);
    Constants.PROPS.setProperty(Constants.PROP_LAST_OPEN_DIR, fc.getCurrentDirectory().getPath());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      if (frame.openFileName != null) {
        saveText();
        frame.openFileName = null;
      }

      File selectedFile = fc.getSelectedFile();
//      if (!selectedFile.exists()) {
//        try {
//          selectedFile.createNewFile();
//        } catch (IOException ex) {
//          ex.printStackTrace();
//        }
//      }
      load(selectedFile);

    }

  }

  public void load(File selectedFile) {
    frame.openFileName = selectedFile.getAbsolutePath();
    frame.shortFileName = selectedFile.getName();
    DMLProject sp = new DMLProject();
    if (selectedFile.exists()) {
      try {
        FileInputStream fis = new FileInputStream(selectedFile);
        BufferedInputStream bis = new BufferedInputStream(fis);
        XMLDecoder xmlDecoder = new XMLDecoder(bis);
        sp = (DMLProject) xmlDecoder.readObject();
      } catch (Exception e1) {
        e1.printStackTrace();
      }
    } else {
      //new file
      if (!frame.shortFileName.toLowerCase().endsWith(".xml")) {
        frame.shortFileName += ".xml";
        frame.openFileName += ".xml";
      }
    }
    frame.hash = Constants.getHash(serialize(sp));
    frame.editorPanel.setData(sp);
    frame.setTitle(frame.openFileName);
    ((CardLayout) frame.cards.getLayout()).show(frame.cards, Constants.PROP_EDITOR_COMPONENT);
  }
}